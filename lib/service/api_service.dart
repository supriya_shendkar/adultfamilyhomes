import 'dart:convert';
import 'package:afh/model/tour_req.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';

Future<SaveTourRequest> tour(String url, {map}) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  return http.post(url, body: utf8.encode(json.encode(map)), headers: {
    "content-type": "application/json",
    "Accept": "application/json",
    "access-control-allow-origin": "*",
    "Authorization": "Bearer " + prefs.getString("token"),
  }).then((http.Response response) {
    final int statusCode = response.statusCode;
    print(statusCode);
    return SaveTourRequest.fromJson(json.decode(response.body));
  });
}
class CallsAndMessagesService {
  void call(String number) => launch("tel:$number");
}