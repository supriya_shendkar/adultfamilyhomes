import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import '../constant.dart';

class CompareScreen extends StatefulWidget {
  @override
  _CompareScreenState createState() => _CompareScreenState();
}

class _CompareScreenState extends State<CompareScreen> {
  List cityList = List();
  List homeList = List();
  List userHouseList = new List();
  String _selectedCity1;
  String _selectedCity2;
  String _selectedHome1;
  String _selectedHome2;
  @override
  void initState() {
    super.initState();
    this.getAllCities();
  }

  Future<String> getAllCities() async {
    var response = await http.get(
        Uri.encodeFull(BASE_URL + "/api/myafh/getAllCities"),
        headers: {"Accept": "application/json"});
    if (this.mounted) {
      setState(() {
        var convertDataToJson = json.decode(response.body);
        cityList = convertDataToJson['allCities'];
        cityList.sort();
      });
    }
    return "Success";
  }

  Future<String> getCityWiseData1() async {
    var response = await http.get(
        Uri.encodeFull(BASE_URL + "/home/filtered_list?city=" + _selectedCity1),
        headers: {"Accept": "application/json"});
    if (this.mounted) {
      setState(() {
        var convertDataToJson = json.decode(response.body);
        var allData = convertDataToJson['listFilterPojo'];
        homeList = allData
            .where((item) => item['userCity'] == _selectedCity1)
            .toList();
        userHouseList = [];
        homeList.forEach((home) => {userHouseList.add(home['userHouseName'])});
      });
    }
    return "Success";
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: BackButton(color: Colors.white),
        title: Text(
          'Compare',
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Container(
              padding: EdgeInsets.all(10),
              child: Text(
                'Info! Choose at least two cities of your choice to see how they compare on amenities, activities & categories.',
                style: TextStyle(fontSize: 20),
              ),
            ),
            Container(
              color: Colors.white,
              child: Table(
                  defaultColumnWidth: FixedColumnWidth(60.0),
                  border: TableBorder.all(color: Colors.grey[400]),
                  children: [
                    TableRow(children: [
                      Padding(
                          padding: EdgeInsets.symmetric(
                              vertical: 15, horizontal: 40),
                          child: Text('City')),
                      DropdownButton(
                        elevation: 8,
                        underline: Container(color: Colors.white),
                        iconEnabledColor: Colors.black,
                        isExpanded: true,
                        items: cityList
                            .map(
                              (value) => DropdownMenuItem(
                                child: Padding(
                                  padding: EdgeInsets.only(left: 5),
                                  child: Text(
                                    value,
                                    style: TextStyle(fontSize: 14),
                                  ),
                                ),
                                value: value,
                              ),
                            )
                            .toList(growable: true),
                        value: _selectedCity1,
                        onChanged: (newVal) {
                          setState(() {
                            _selectedCity1 = newVal;
                            getCityWiseData1();
                            _selectedHome1=null;
                          });
                        },

                        hint: Padding(
                            padding: EdgeInsets.only(left: 10),
                            child: Text('Select City',
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(fontSize: 14))),
                      ),
                      DropdownButton(
                        elevation: 8,
                        underline: Container(color: Colors.white),
                        isExpanded: true,
                        iconEnabledColor: Colors.black,
                        items: cityList
                            .map(
                              (value) => DropdownMenuItem(
                                child: Padding(
                                  padding: EdgeInsets.only(left: 5),
                                  child: Text(
                                    value,
                                    style: TextStyle(fontSize: 14),
                                  ),
                                ),
                                value: value,
                              ),
                            )
                            .toList(growable: true),
                        value: _selectedCity2,
                        onChanged: (newVal) {
                          setState(() {
                            _selectedCity2 = newVal;
                            getCityWiseData1();
                            _selectedHome1=null;
                          });
                        },

                        hint: Padding(
                          padding: EdgeInsets.only(left: 10),
                          child: Text('Select City',
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(fontSize: 14)),
                        ),
                      ),
                    ]),
                  ]),
            ),
            Container(
              color: Colors.white,
              child: Table(
                  defaultColumnWidth: FixedColumnWidth(60.0),
                  border: TableBorder.all(color: Colors.grey[400]),
                  children: [
                    TableRow(children: [
                      Padding(
                          padding: EdgeInsets.symmetric(
                              vertical: 15, horizontal: 20),
                          child: Text('AFH Home')),
                      Container(
                        child: _addSecondDropdown1(),
                      ),
                      Container(
                        child: _addSecondDropdown2(),
                      ),
                    ]),
                  ]),
            ),
            SizedBox(
              height: 15,
            ),
            Container(
              color: Colors.white,
              padding: EdgeInsets.all(10),
              child: Center(
                child: Text(
                  'Room Types',
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                ),
              ),
            ),
            Container(
              color: Colors.white,
              child: Table(
                  defaultColumnWidth: FixedColumnWidth(60.0),
                  border: TableBorder.all(color: Colors.grey[400]),
                  children: [
                    _buildTableRow("Private Room, , ,"),
                    _buildTableRow("Semi-private Room, , ,"),
                    _buildTableRow("Room & Bath, , ,"),
                    _buildTableRow("Room & Shower, , ,"),
                  ]),
            ),
            SizedBox(
              height: 20,
            ),
            Container(
              color: Colors.white,
              padding: EdgeInsets.all(10),
              child: Center(
                child: Text(
                  'AFH Services',
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                ),
              ),
            ),
            Container(
              color: Colors.white,
              child: Table(
                  defaultColumnWidth: FixedColumnWidth(60.0),
                  border: TableBorder.all(color: Colors.grey[400]),
                  children: [
                    _buildTableRow("24 hour care, , ,"),
                    _buildTableRow("Massage Therapist, , ,"),
                    _buildTableRow("RN Operated, , ,"),
                    _buildTableRow("Dementia/Alzheimer's, , ,"),
                    _buildTableRow("Parkinson's, , ,"),
                    _buildTableRow("Oxygen Therapy, , ,"),
                    _buildTableRow("Diabetes, , ,"),
                    _buildTableRow("Developmental Disabilities, , ,"),
                    _buildTableRow("Visual/Hearing Impaired, , ,"),
                    _buildTableRow("Hospice, , ,"),
                    _buildTableRow("PT/OT & Sp. Therapy, , ,"),
                    _buildTableRow("Stroke, , ,"),
                    _buildTableRow("Wound Care, , ,"),
                    _buildTableRow("Respite Care-Short-term, , ,"),
                    _buildTableRow("Awake Night Staff, , ,"),
                    _buildTableRow("Emergency call buttons, , ,"),
                    _buildTableRow("Housekeeping/Laundry, , ,"),
                    _buildTableRow("Asst. with Daily Living, , ,"),
                    _buildTableRow("Medication Management, , ,"),
                    _buildTableRow("Podiatrist, , ,"),
                    _buildTableRow("Medication Delivery, , ,"),
                    _buildTableRow("Pet Care, , ,"),
                    _buildTableRow("Generator, , ,"),
                  ]),
            ),
            SizedBox(
              height: 15,
            ),
            Container(
              color: Colors.white,
              padding: EdgeInsets.all(10),
              child: Center(
                child: Text(
                  'AFH Activities and Amenities',
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                ),
              ),
            ),
            Container(
              color: Colors.white,
              child: Table(
                  defaultColumnWidth: FixedColumnWidth(60.0),
                  border: TableBorder.all(color: Colors.grey[400]),
                  children: [
                    _buildTableRow("Activities coordinator, , ,"),
                    _buildTableRow("Arts and Crafts, , ,"),
                    _buildTableRow("Music, , ,"),
                    _buildTableRow("Games, , ,"),
                    _buildTableRow("Birthday celebration, , ,"),
                    _buildTableRow("Scenic Drive, , ,"),
                    _buildTableRow("Library on Wheels, , ,"),
                    _buildTableRow("Wheelchair accessible, , ,"),
                    _buildTableRow("Generator, , ,"),
                    _buildTableRow("Cable TV, , ,"),
                    _buildTableRow("Internet access, , ,"),
                    _buildTableRow("Telephone, , ,"),
                    _buildTableRow("Newspaper, , ,"),
                  ]),
            ),
          ],
        ),
      ),
    );
  }

  Widget _addSecondDropdown1() {
    return _selectedCity1 != null
        ? DropdownButton(
            elevation: 8,
            isExpanded: true,
            underline: Container(
              color: Colors.white,
            ),
            iconEnabledColor: Colors.black,
            items: userHouseList
                .map((homeName) => DropdownMenuItem<String>(
                    child: new Padding(
                      padding: EdgeInsets.only(left: 5),
                      child: Text(
                        homeName,
                        style: TextStyle(fontSize: 12),
                      ),
                    ),
                    value: homeName))
                .toList(growable: true),
          value: _selectedHome1,
            onChanged: (newValue) {
              setState(() {
                _selectedHome1 = newValue;
              });
            },
            hint: Padding(
                padding: EdgeInsets.only(left: 5),
                child: Text(
                  'Select Home',
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(fontSize: 12),
                )),
          )
        : Container(
            child: Padding(
                padding: EdgeInsets.symmetric(vertical: 15, horizontal: 10),
                child: Text('Select city first')),
          ); // Return an empty Container instead.
  }

  Widget _addSecondDropdown2() {
    return _selectedCity2 != null
        ? DropdownButton(
            elevation: 8,
            isExpanded: true,
            underline: Container(
              color: Colors.white,
            ),
            iconEnabledColor: Colors.black,
            items: userHouseList
                .map((homeName) => DropdownMenuItem<String>(
                    child: new Padding(
                      padding: EdgeInsets.only(left: 5),
                      child: Text(
                        homeName,
                        style: TextStyle(fontSize: 12),
                      ),
                    ),
                    value: homeName))
                .toList(growable: true),
            onChanged: (newValue) {
              setState(() {
                _selectedHome2 = newValue;
              });
            },
            value: _selectedHome2,
            hint: Padding(
                padding: EdgeInsets.only(left: 5),
                child: Text(
                  'Select Home',
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(fontSize: 12),
                )),
          )
        : Container(
            child: Padding(
              padding: EdgeInsets.symmetric(vertical: 15, horizontal: 10),
              child: Text('Select city first'),
            ),
          );
  }

  TableRow _buildTableRow(String s) {
    return TableRow(
      children: s.split(',').map((name) {
        return Container(
          child: Text(name, style: TextStyle(fontSize: 10.0)),
          padding: EdgeInsets.all(8.0),
        );
      }).toList(),
    );
  }
}
