
import 'package:flutter/material.dart';
import 'package:grouped_buttons/grouped_buttons.dart';

import '../constant.dart';

class FilterScreen extends StatefulWidget {
  var convertDataToJson;
  FilterScreen({Key key, this.convertDataToJson}) : super(key: key);

  @override
  _FilterScreenState createState() => _FilterScreenState();
}

class _FilterScreenState extends State<FilterScreen> {
  @override
  void initState() {
    super.initState();
    var allData = widget.convertDataToJson;
    var homeCount = allData['listFilterPojo'].length;
    room = new List.filled(homeCount, 1);
    amenity = new List.filled(homeCount, 1);
    activity = new List.filled(homeCount, 1);
    medicAid = new List.filled(homeCount, 1);
    arrFinal = new List.filled(homeCount, 1);
  }

  bool _medicAid = false;
  var roomName = [];
  var amenityName = [];
  var activityName = [];
  var arrRoom = [];
  var arrAmen = [];
  var arrAct = [];
  var room = [];
  var activity = [];
  var amenity = [];
  var medicAid = [];
  var arrFinal = [];

  roomFilter() {
    var allData = widget.convertDataToJson;
    var homeCount = allData['listFilterPojo'].length;
    arrRoom = new List(homeCount);
    if (roomName.length != 0) {
      var checkedRoom = roomName.length;
      arrRoom = new List.filled(homeCount, 0);
      for (int i = 0; i < homeCount; i++) {
        for (int j = 0; j < checkedRoom; j++) {
          var room1 = roomName[j];
          var data = allData['rooms'];
          print(data);
          var a = data[room1];
          if (a[i] == "1" || arrRoom[i] == 1) {
            arrRoom[i] = 1;
          } else {
            arrRoom[i] = 0;
          }
        }
        room = arrRoom;
      }
    } else {
      room = new List.filled(homeCount, 1);
    }
  }

  amenityFilter() {
    var allData = widget.convertDataToJson;
    var homeCount = allData['listFilterPojo'].length;
    arrAmen = new List(homeCount);
    arrAmen = new List.filled(homeCount, 1);
    if (amenityName.length != 0) {
      var checkedAmen = amenityName.length;
      for (int i = 0; i < homeCount; i++) {
        for (int j = 0; j < checkedAmen; j++) {
          var amenity1 = amenityName[j];
          var data1 = allData['amenities'];
          print(data1);
          var a = data1[amenity1];
          print(a[i]);
          if (a[i] == "1" && arrAmen[i] == 1) {
            arrAmen[i] = 1;
          } else {
            arrAmen[i] = 0;
          }
        }
        amenity = arrAmen;
      }
    } else {
      amenity = new List.filled(homeCount, 1);
    }
  }

  activityFilter() {
    var allData = widget.convertDataToJson;
    var homeCount = allData['listFilterPojo'].length;
    arrAct = new List(homeCount);
    if (activityName.length != 0) {
      var checkedAmen = activityName.length;
      arrAct = new List.filled(homeCount, 1);
      for (int i = 0; i < homeCount; i++) {
        for (int j = 0; j < checkedAmen; j++) {
          var activity1 = activityName[j];
          var data1 = allData['activities'];
          print(data1);
          var a = data1[activity1];
          print(a[i]);
          if (a[i] == "1" && arrAct[i] == 1) {
            arrAct[i] = 1;
          } else {
            arrAct[i] = 0;
          }
        }
        activity = arrAct;
      }
    } else {
      activity = new List.filled(homeCount, 1);
    }
  }

  finalFilter() {
    var allData = widget.convertDataToJson;
    var homeCount = allData['listFilterPojo'].length;
    var arrFinal = new List(homeCount);
    if (room.length == 0) {
      for (int i = 0; i < homeCount; i++) {
        room[i] = 1;
      }
    }
    if (activity.length == 0) {
      for (int i = 0; i < homeCount; i++) {
        activity[i] = 1;
      }
    }
    if (amenity.length == 0) {
      for (int i = 0; i < homeCount; i++) {
        amenity[i] = 1;
      }
    }
    for (int i = 0; i < homeCount; i++) {
      if (room[i] == 1 && activity[i] == 1 && amenity[i] == 1) {
        arrFinal[i] = 1;
      } else {
        arrFinal[i] = 0;
      }
    }

    if (_medicAid == true) {
      for (int i = 0; i < arrFinal.length; i++) {
        var data = allData['listFilterPojo'][i];
        if (data['medicAid'] == 'yes') {
          if (arrFinal[i] == 1) {
            arrFinal[i] = 1;
          } else {
            arrFinal[i] = 0;
          }
        } else {
          arrFinal[i] = 0;
        }
      }
    }
    Navigator.pop(context, arrFinal);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: Colors.white,
          ),
          onPressed: () => Navigator.pop(context, arrFinal),
        ),
        title: Text(
          'Filter',
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Container(
                color: Colors.white,
                padding: EdgeInsets.fromLTRB(20, 0, 0, 0),
                child: Row(
                  children: <Widget>[
                    Checkbox(
                      checkColor: Colors.white,
                      value: _medicAid,
                      activeColor: Theme.of(context).primaryColorDark,
                      onChanged: (bool value) {
                        setState(() {
                          _medicAid = value;
                        });
                      },
                    ),
                    Text(
                      'MedicAid',
                      style: TextStyle(
                        fontSize: 20,
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                color: Colors.white,
                child: Padding(
                  padding: EdgeInsets.fromLTRB(5, 5, 0, 0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        'Room types',
                        style: TextStyle(
                            fontSize: 22, fontWeight: FontWeight.bold),
                      ),
                      CheckboxGroup(
                        checkColor: Colors.white,
                        activeColor: Theme.of(context).primaryColorDark,
                        labels: <String>[
                          "Private Room",
                          "Semi Private Room",
                          "Room & Bath",
                          "Room & Shower",
                          "Shared Main Shower"
                        ],
                        checked: roomFilter(),
                        onSelected: (List<String> checked) {
                          setState(() {
                            if (checked.contains("Private Room")) {
                              roomName.add("priv_room");
                            } else {
                              roomName.remove("priv_room");
                            }
                            if (checked.contains("Semi Private Room")) {
                              roomName.add("semi_priv_room");
                            } else {
                              roomName.remove("semi_priv_room");
                            }
                            if (checked.contains("Room & Bath")) {
                              roomName.add("room_bath");
                            } else {
                              roomName.remove("room_bath");
                            }
                            if (checked.contains("Room & Shower")) {
                              roomName.add("room_showe");
                            } else {
                              roomName.remove("room_showe");
                            }
                            if (checked.contains("Shared Main Shower")) {
                              roomName.add("shar_main_shower");
                            } else {
                              roomName.remove("shar_main_shower");
                            }
                          });
                        },
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                color: Colors.white,
                height: 870,
                child: Padding(
                  padding: EdgeInsets.fromLTRB(5, 5, 0, 0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        'Services',
                        style: TextStyle(
                            fontSize: 22, fontWeight: FontWeight.bold),
                      ),
                      Expanded(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Expanded(
                              child: CheckboxGroup(
                                checkColor: Colors.white,
                                activeColor: Theme.of(context).primaryColorDark,
                                labels: <String>[
                                  "Podiatrist",
                                  "Catheter Care \nProvided",
                                  "PT/OT & \nSp.Therapy",
                                  "Doctor On Site",
                                  "Awake\nNight Staff",
                                  "Developmental \nDisabilities",
                                  "Pet Care",
                                  "Diabetes",
                                  "3 caregiver \non shift",
                                  "Transportation \nto Shopping \nProvided",
                                  "Home Doctor",
                                  "Medicaid \nAccepted",
                                  "Wound Care",
                                  "2 caregivers \non shift",
                                  "Feeding Tube \nResident's \nWelcomed",
                                  "Male Residents \nPreferred",
                                  "Medication \nDelivery"
                                ],
                                checked: amenityFilter(),
                                onSelected: (List<String> checked) {
                                  setState(() {
                                    if (checked.contains("Podiatrist")) {
                                      amenityName.add("podiatrist");
                                    } else {
                                      amenityName.remove("podiatrist");
                                    }
                                    if (checked
                                        .contains("Catheter Care \nProvided")) {
                                      amenityName.add("cath_care_prov");
                                    } else {
                                      amenityName.remove("cath_care_prov");
                                    }
                                    if (checked
                                        .contains("PT/OT &\nSp.Therapy")) {
                                      amenityName.add("ptot_spther");
                                    } else {
                                      amenityName.remove("ptot_spther");
                                    }
                                    if (checked.contains("Doctor On Site")) {
                                      amenityName.add("doc_onsite");
                                    } else {
                                      amenityName.remove("doc_onsite");
                                    }
                                    if (checked
                                        .contains("Awake\nNight Staff")) {
                                      amenityName.add("awak_nig_staf");
                                    } else {
                                      amenityName.remove("awak_nig_staf");
                                    }
                                    if (checked.contains(
                                        "Developmental \nDisabilities")) {
                                      amenityName.add("dev_disab");
                                    } else {
                                      amenityName.remove("dev_disab");
                                    }
                                    if (checked.contains("Pet Care")) {
                                      amenityName.add("pet_care");
                                    } else {
                                      amenityName.remove("pet_care");
                                    }
                                    if (checked.contains("Diabetes")) {
                                      amenityName.add("diabe");
                                    } else {
                                      amenityName.remove("diabe");
                                    }
                                    if (checked
                                        .contains("3 caregiver \non shift")) {
                                      amenityName.add("caregi_3_on_shif");
                                    } else {
                                      amenityName.remove("caregi_3_on_shif");
                                    }
                                    if (checked.contains(
                                        "Transportation \nto Shopping \nProvided")) {
                                      amenityName
                                          .add("transp_to_shop_provided");
                                    } else {
                                      amenityName
                                          .remove("transp_to_shop_provided");
                                    }
                                    if (checked.contains("Home Doctor")) {
                                      amenityName.add("home_doc");
                                    } else {
                                      amenityName.remove("home_doc");
                                    }
                                    if (checked
                                        .contains("Medicaid \nAccepted")) {
                                      amenityName.add("medicai_accep");
                                    } else {
                                      amenityName.remove("medicai_accep");
                                    }
                                    if (checked.contains("Wound Care")) {
                                      amenityName.add("woun_care");
                                    } else {
                                      amenityName.remove("woun_care");
                                    }
                                    if (checked
                                        .contains("2 caregivers \non shift")) {
                                      amenityName.add("caregi_2_on_shif");
                                    } else {
                                      amenityName.remove("caregi_2_on_shif");
                                    }
                                    if (checked.contains(
                                        "Feeding Tube \nResident's \nWelcomed")) {
                                      amenityName.add("feed_tube_resi_welc");
                                    } else {
                                      amenityName.remove("feed_tube_resi_welc");
                                    }
                                    if (checked.contains(
                                        "Male Residents \nPreferred")) {
                                      amenityName.add("male_resid_pref");
                                    } else {
                                      amenityName.remove("male_resid_pref");
                                    }
                                    if (checked
                                        .contains("Medication \nDelivery")) {
                                      amenityName.add("medic_deliv");
                                    } else {
                                      amenityName.remove("medic_deliv");
                                    }
                                  });
                                },
                              ),
                            ),
                            Expanded(
                              child: CheckboxGroup(
                                checkColor: Colors.white,
                                activeColor: Theme.of(context).primaryColorDark,
                                labels: <String>[
                                  "Dementia/"
                                      "\nAlzheimer's",
                                  "Hospice",
                                  "Female \nResidents \nPreferred",
                                  "Medication \nManagement",
                                  "Massage \nTherapist",
                                  "Home owner \nlives in the \nhouse & provide \ncare",
                                  "Respite Care\nShort - term",
                                  "Oxygen Therapy",
                                  "Stroke",
                                  "Colostomy Bag \nResidents",
                                  "Transportation \nto Doctor's \nProvided",
                                  "24 hour care",
                                  "Visual/Hearing \nImpaired",
                                  "RN Operated",
                                  "Housekeeping/\nLaundry",
                                  "Parkinson's",
                                  "Assistance with\nDaily Living",
                                ],
                                checked: amenityFilter(),
                                onSelected: (List<String> checked) {
                                  setState(() {
                                    if (checked
                                        .contains("Dementia/\nAlzheimer's")) {
                                      amenityName.add("deme_alzi");
                                    } else {
                                      amenityName.remove("deme_alzi");
                                    }
                                    if (checked.contains("Hospice")) {
                                      amenityName.add("hospice");
                                    } else {
                                      amenityName.remove("hospice");
                                    }
                                    if (checked.contains(
                                        "Female \nResidents \nPreferred")) {
                                      amenityName.add("femal_resid_pref");
                                    } else {
                                      amenityName.remove("femal_resid_pref");
                                    }
                                    if (checked
                                        .contains("Medication \nManagement")) {
                                      amenityName.add("medicat_manag");
                                    } else {
                                      amenityName.remove("medicat_manag");
                                    }
                                    if (checked
                                        .contains("Massage \nTherapist")) {
                                      amenityName.add("mass_thera");
                                    } else {
                                      amenityName.remove("mass_thera");
                                    }
                                    if (checked.contains(
                                        "Home owner \nlives in the \nhouse & provide \ncare")) {
                                      amenityName.add("owne_provides_care");
                                    } else {
                                      amenityName.remove("owne_provides_care");
                                    }
                                    if (checked.contains(
                                        "Respite Care\nShort - term")) {
                                      amenityName.add("resp_care_short");
                                    } else {
                                      amenityName.remove("resp_care_short");
                                    }
                                    if (checked.contains("Oxygen Therapy")) {
                                      amenityName.add("oxy_ther");
                                    } else {
                                      amenityName.remove("oxy_ther");
                                    }
                                    if (checked.contains("Stroke")) {
                                      amenityName.add("stroke");
                                    } else {
                                      amenityName.remove("stroke");
                                    }
                                    if (checked.contains(
                                        "Colostomy Bag \nResidents")) {
                                      amenityName.add("colos_bag_resi");
                                    } else {
                                      amenityName.remove("colos_bag_resi");
                                    }

                                    if (checked.contains(
                                        "Transportation \nto Doctor's \nProvided")) {
                                      amenityName.add("transp_to_doc_prov");
                                    } else {
                                      amenityName.remove("transp_to_doc_prov");
                                    }
                                    if (checked.contains("24 hour care")) {
                                      amenityName.add("care24");
                                    } else {
                                      amenityName.remove("care24");
                                    }
                                    if (checked.contains(
                                        "Visual/Hearing \nImpaired")) {
                                      amenityName.add("vis_hear_impa");
                                    } else {
                                      amenityName.remove("vis_hear_impa");
                                    }
                                    if (checked.contains("RN Operated")) {
                                      amenityName.add("rn_operated");
                                    } else {
                                      amenityName.remove("rn_operated");
                                    }
                                    if (checked
                                        .contains("Housekeeping/\nLaundry")) {
                                      amenityName.add("houskep_laundry");
                                    } else {
                                      amenityName.remove("houskep_laundry");
                                    }
                                    if (checked.contains("Parkinson's")) {
                                      amenityName.add("parkins");
                                    } else {
                                      amenityName.remove("parkins");
                                    }
                                    if (checked.contains(
                                        "Assistance with\nDaily Living")) {
                                      amenityName.add("asst_dail_liv");
                                    } else {
                                      amenityName.remove("asst_dail_liv");
                                    }
                                  });
                                },
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                color: Colors.white,
                height: 490,
                child: Padding(
                  padding: EdgeInsets.fromLTRB(5, 5, 0, 0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        'Activities',
                        style: TextStyle(
                            fontSize: 22, fontWeight: FontWeight.bold),
                      ),
                      Expanded(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Expanded(
                              child: CheckboxGroup(
                                checkColor: Colors.white,
                                activeColor: Theme.of(context).primaryColorDark,
                                labels: <String>[
                                  "Telephone",
                                  "Arts and Crafts",
                                  "Newspaper",
                                  "Activities \n coordinator",
                                  "Generator",
                                  "Wheelchair \naccessible",
                                  "Tv in room",
                                  "Hoyer Lift",
                                  "Hospital \nElelectric bed \nAvailable in \nthe room",
                                ],
                                checked: activityFilter(),
                                onSelected: (List<String> checked) {
                                  setState(() {
                                    if (checked.contains("Telephone")) {
                                      activityName.add("telepho");
                                    } else {
                                      activityName.remove("telepho");
                                    }
                                    if (checked.contains("Arts and Crafts")) {
                                      activityName.add("art_craf");
                                    } else {
                                      activityName.remove("art_craf");
                                    }
                                    if (checked.contains("Newspaper")) {
                                      activityName.add("newspap");
                                    } else {
                                      activityName.remove("newspap");
                                    }
                                    if (checked.contains(
                                        "Activities \n coordinator")) {
                                      activityName.add("acti_coord");
                                    } else {
                                      activityName.remove("acti_coord");
                                    }
                                    if (checked.contains("Generator")) {
                                      activityName.add("generator");
                                    } else {
                                      activityName.remove("generator");
                                    }
                                    if (checked
                                        .contains("Wheelchair \naccessible")) {
                                      activityName.add("wheelch_accessib");
                                    } else {
                                      activityName.remove("wheelch_accessib");
                                    }
                                    if (checked.contains("Tv in room")) {
                                      activityName.add("tv_in_room");
                                    } else {
                                      activityName.remove("tv_in_room");
                                    }
                                    if (checked.contains("Hoyer Lift")) {
                                      activityName.add("hoy_lift");
                                    } else {
                                      activityName.remove("hoy_lift");
                                    }
                                    if (checked.contains(
                                        "Hospital \nElelectric bed \nAvailable in \nthe room")) {
                                      activityName.add("hosp_electric_bed");
                                    } else {
                                      activityName.remove("hosp_electric_bed");
                                    }
                                  });
                                },
                              ),
                            ),
                            Expanded(
                              child: CheckboxGroup(
                                checkColor: Colors.white,
                                activeColor: Theme.of(context).primaryColorDark,
                                labels: <String>[
                                  "Library on \n Wheels",
                                  "Internet access",
                                  "Music",
                                  "Outdoor \nCommon Area",
                                  "Beautician",
                                  "Birthday \n celebration",
                                  "Games",
                                  "Emergency \ncall buttons",
                                  "Cable TV",
                                ],
                                checked: activityFilter(),
                                onSelected: (List<String> checked) {
                                  setState(() {
                                    if (checked
                                        .contains("Library on \n Wheels")) {
                                      activityName.add("lib_onwhel");
                                    } else {
                                      activityName.remove("lib_onwhel");
                                    }
                                    if (checked.contains("Internet access")) {
                                      activityName.add("inter_acce");
                                    } else {
                                      activityName.remove("inter_acce");
                                    }
                                    if (checked.contains("Music")) {
                                      activityName.add("music");
                                    } else {
                                      activityName.remove("music");
                                    }
                                    if (checked
                                        .contains("Outdoor \nCommon Area")) {
                                      activityName.add("outd_com_area");
                                    } else {
                                      activityName.remove("outd_com_area");
                                    }

                                    if (checked.contains("Beautician")) {
                                      activityName.add("beauti");
                                    } else {
                                      activityName.remove("beauti");
                                    }
                                    if (checked
                                        .contains("Birthday \n celebration")) {
                                      activityName.add("birt_celeb");
                                    } else {
                                      activityName.remove("birt_celeb");
                                    }

                                    if (checked.contains("Games")) {
                                      activityName.add("games");
                                    } else {
                                      activityName.remove("games");
                                    }
                                    if (checked
                                        .contains("Emergency \ncall buttons")) {
                                      activityName.add("emer_call_butt");
                                    } else {
                                      activityName.remove("emer_call_butt");
                                    }
                                    if (checked.contains("Cable TV")) {
                                      activityName.add("cabl_tv");
                                    } else {
                                      activityName.remove("cabl_tv");
                                    }
                                  });
                                },
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              /* SizedBox(
                height: 10,
              ),
              Container(
                color: Colors.white,
                child: Padding(
                  padding: EdgeInsets.fromLTRB(5, 5, 0, 0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        'Distance',
                        style: TextStyle(
                            fontSize: 22, fontWeight: FontWeight.bold),
                      ),
                      CheckboxGroup(
                        labels: <String>[
                          "1 miles to 3 miles",
                          "3 miles to 6 miles",
                          "7 miles to 10 miles",
                          "15 and more"
                        ],
                        onSelected: (List<String> checked) => print(
                          checked.toString(),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                color: Colors.white,
                child: Padding(
                  padding: EdgeInsets.fromLTRB(5, 5, 0, 0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        'User Ratings',
                        style: TextStyle(
                            fontSize: 22, fontWeight: FontWeight.bold),
                      ),
                      Container(
                        padding: EdgeInsets.only(right: 5, left: 5, bottom: 10),
                        child: Row(
                          children: <Widget>[
                            Expanded(
                              child: Card(
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Text(
                                      '1',
                                      style: TextStyle(fontSize: 22),
                                    ),
                                    Icon(
                                      Icons.star,
                                      color: Colors.yellow,
                                      size: 30,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Expanded(
                              child: Card(
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Text(
                                      '2',
                                      style: TextStyle(fontSize: 22),
                                    ),
                                    Icon(
                                      Icons.star,
                                      color: Colors.yellow,
                                      size: 30,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Expanded(
                              child: Card(
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Text(
                                      '3',
                                      style: TextStyle(fontSize: 22),
                                    ),
                                    Icon(
                                      Icons.star,
                                      color: Colors.yellow,
                                      size: 30,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Expanded(
                              child: Card(
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Text(
                                      '4',
                                      style: TextStyle(fontSize: 22),
                                    ),
                                    Icon(
                                      Icons.star,
                                      color: Colors.yellow,
                                      size: 30,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Expanded(
                              child: Card(
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Text(
                                      '5',
                                      style: TextStyle(fontSize: 22),
                                    ),
                                    Icon(
                                      Icons.star,
                                      color: Colors.yellow,
                                      size: 30,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),*/
            ],
          ),
        ),
      ),
      bottomNavigationBar: Container(
        height: 50,
        child: Row(
          children: <Widget>[
            Expanded(
              child: GestureDetector(
                onTap: () {
                  finalFilter();
                },
                child: Container(
                  color: Theme.of(context).primaryColor,
                  child: Center(
                    child: Text('APPLY', style: bottomButtonTextStyle),
                  ),
                ),
              ),
            ),
            /*SizedBox(
              width: 2,
            ),
            Expanded(
              child: Container(
                color: Theme.of(context).primaryColor,
                child: Center(
                  child: Text('RESET', style: bottomButtonTextStyle),
                ),
              ),
            ),*/
          ],
        ),
      ),
    );
  }
}
