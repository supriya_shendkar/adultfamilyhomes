import 'package:afh/profile/loginpage.dart';
import 'package:afh/profile/wishlist_homes.dart';
import 'package:afh/third_page/third_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:page_transition/page_transition.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'compare_screen.dart';
import 'dart:async';
import 'package:http/http.dart' as http;
import 'filter_screen.dart';
import '../constant.dart';


class SecondPageScreen extends StatefulWidget {
  final String cityName;
  SecondPageScreen({@required this.cityName});
  @override
  _SecondPageScreenState createState() => _SecondPageScreenState();
}

class _SecondPageScreenState extends State<SecondPageScreen>
    with SingleTickerProviderStateMixin {
  List myData = List();
  List bookMarkData = List();
  var response;
  var convertDataToJson;
  List finalData = List();
  var color = Colors.red;
  Set<String> bookedStayIds = Set();
  List bookMarks = List();

  @override
  void initState() {
    super.initState();
    this.getCityWiseData();
  }

  void checkStatus() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (prefs.getString('userID') != null) {
      Navigator.push(
        context,
        PageTransition(
            type: PageTransitionType.rightToLeft, child: WishListHomes()),
      );
    } else {
      Navigator.push(
          context,
          PageTransition(
              type: PageTransitionType.rightToLeft, child: LoginPage()));
    }
  }

  Future<String> getCityWiseData() async {
    response = await http.get(
        Uri.encodeFull(
            BASE_URL + "/home/filtered_list?city=" + widget.cityName),
        headers: {"Accept": "application/json"});
    if (this.mounted) {
      setState(() {
        convertDataToJson = json.decode(response.body);
        myData = convertDataToJson['listFilterPojo'];

        var list1 = myData
            .where((item) => item['subscriptionStatus'] == "pro")
            .toList();
        var list2 = myData
            .where((item) => item['subscriptionStatus'] == "plus")
            .toList();
        var list3 = myData
            .where((item) => item['subscriptionStatus'] == "basic")
            .toList();
        finalData = list1 + list2 + list3;
      });
    }
    return "Success";
  }

  Future<String> favouriteHome(String stayId) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var bookMarksResponse = await http.get(
        Uri.encodeFull(
            BASE_URL + "/api/myafh/owner/bookHomeMark?stayId=" + stayId),
        headers: {
          "Accept": "application/json",
          "access-control-allow-origin": "*",
          "Authorization": "Bearer " + prefs.getString("token"),
        });
    setState(() {
      var jsonData = json.decode(bookMarksResponse.body);
      jsonData['result'] == 'success' ? bookedStayIds.add(stayId) : "";
      print(bookedStayIds);
    });
    return "Success";
  }

  Future<String> unFavouriteHome(String stayId) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var bookMarksResponse = await http.get(
        Uri.encodeFull(
            BASE_URL + "/api/myafh/owner/unBookMarkHome?stayId=" + stayId),
        headers: {
          "Accept": "application/json",
          "access-control-allow-origin": "*",
          "Authorization": "Bearer " + prefs.getString("token"),
        });
    setState(() {
      var jsonData = json.decode(bookMarksResponse.body);
      jsonData['result'] == 'success' ? bookedStayIds.remove(stayId) : null;
    });
    print(bookedStayIds);
    return "Success";
  }

  void bookUnBook(String stayId) {
    if (bookedStayIds.contains(stayId)) {
      unFavouriteHome(stayId);
    } else {
      favouriteHome(stayId);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: BackButton(color: Colors.white),
        title: Text(
          widget.cityName,
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: SafeArea(
          child: response == null
              ? Center(
                  child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    CircularProgressIndicator(),
                    SizedBox(height: 30),
                    Text('    Loading . . .'),
                  ],
                ))
              : ListView.builder(
                  itemBuilder: (BuildContext context, int index) {
                    return Container(
                      padding: EdgeInsets.fromLTRB(5, 5, 5, 0),
                      child: GestureDetector(
                        onTap: () {
                          Navigator.push(
                            context,
                            PageTransition(
                                type: PageTransitionType.rightToLeft,
                                child: ThirdPage(
                                  stayId: finalData[index]['stayId'],
                                  homeName: finalData[index]['userHouseName'],
                                )),
                          );
                        },
                        child: new Card(
                          elevation: 20,
                          child: new Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: <Widget>[
                              Stack(
                                children: <Widget>[
                                  ClipRRect(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(4)),
                                    child: new Image.network(
                                      "" + finalData[index]['imagePath'],
                                      fit: BoxFit.fill,
                                      height: 200,
                                      width: 345,
                                    ),
                                  ),
                                  Positioned(
                                    right: 15,
                                    top: 15,
                                    child: IconButton(
                                      icon: Icon(Icons.favorite, size: 30),
                                      color: bookedStayIds.contains(
                                              finalData[index]['stayId'])
                                          ? Colors.red
                                          : Colors.white,
                                      onPressed: () {
                                        bookUnBook(finalData[index]['stayId']);
                                      },
                                    ),
                                  ),
                                  Positioned(
                                      child: finalData[index]
                                                  ['subscriptionStatus'] ==
                                              'pro'
                                          ? Card(
                                              color: Colors.orange,
                                              child: Container(
                                                padding: EdgeInsets.all(5),
                                                child: Text(
                                                  'Premium',
                                                  style: TextStyle(
                                                      color: Colors.white),
                                                ),
                                              ))
                                          : Text('')),
                                ],
                              ),
                              Row(
                                children: <Widget>[
                                  Expanded(
                                    child: Padding(
                                      padding: EdgeInsets.fromLTRB(5, 5, 0, 0),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          new Text(
                                            "" +
                                                finalData[index]
                                                    ['userHouseName'],
                                          ),
                                          SizedBox(
                                            height: 5,
                                          ),
                                          new Text(
                                            "" +
                                                finalData[index]['userAddress'],
                                            style:
                                                TextStyle(color: Colors.grey),
                                          ),
                                          Row(
                                            children: <Widget>[
                                              new IconButton(
                                                icon: Icon(
                                                  Icons.add_box,
                                                  color: Theme.of(context)
                                                      .primaryColorDark,
                                                  size: 30,
                                                ),
                                                onPressed: () {
                                                  Navigator.push(
                                                    context,
                                                    PageTransition(
                                                        type: PageTransitionType
                                                            .rightToLeft,
                                                        child: CompareScreen()),
                                                  );
                                                },
                                              ),
                                              new Text(
                                                'Add to Compare',
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(right: 10),
                                    child: Column(
                                      children: <Widget>[
                                       /* new FlutterRatingBar(
                                          initialRating: finalData[index]
                                              ['rating'],
                                          onRatingUpdate: (rating) {
                                            print(rating);
                                          },
                                          allowHalfRating: true,
                                          fillColor: Colors.amber,
                                          borderColor: Colors.black45,
                                          itemSize: 15,
                                        ),*/
                                       /* SizedBox(
                                          height: 20,
                                        ),*/
                                        Row(
                                          children: <Widget>[
                                            finalData[index]['medicAid'] ==
                                                    'yes'
                                                ? new Image.asset(
                                                    'Icons/medicAid.png',
                                                    scale: 1.3)
                                                : Container(),
                                            SizedBox(width: 5),
                                            finalData[index]['medicAid'] ==
                                                    'yes'
                                                ? new Text(
                                                    'Provides\nMedicAid',
                                                    style: TextStyle(
                                                        color: Theme.of(context)
                                                            .primaryColorDark,
                                                        fontSize: 13),
                                                  )
                                                : Container(),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              )
                            ],
                          ),
                        ),
                      ),
                    );
                  },
                  itemCount: finalData == null ? 0 : finalData.length,
                )),
      bottomNavigationBar: response == null
          ? null
          : Container(
              height: 50,
              color: Colors.white,
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: GestureDetector(
                        child: Container(
                          padding: EdgeInsets.all(5),
                          color: Theme.of(context).primaryColor,
                          child: Column(
                            children: <Widget>[
                              Icon(
                                Icons.filter,
                                color: Colors.white,
                                size: 20,
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Text('FILTER', style: bottomButtonStyle),
                            ],
                          ),
                        ),
                        onTap: () {
                          _sendDataToFilterScreen(context);
                        }),
                  ),
                  SizedBox(
                    width: 2,
                  ),
                  /*Expanded(
                    child: GestureDetector(
                      child: Container(
                        padding: EdgeInsets.all(5),
                        color: Theme.of(context).primaryColor,
                        child: Center(
                          child: Column(
                            children: <Widget>[
                              Icon(
                                Icons.sort,
                                color: Colors.white,
                                size: 20,
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Text('SORT', style: bottomButtonStyle),
                            ],
                          ),
                        ),
                      ),
                      onTap: () {
                        Navigator.push(
                          context,
                          PageTransition(
                            type: PageTransitionType.rightToLeft,
                            child: SortScreen(),
                          ),
                        );
                      },
                    ),
                  ),
                  SizedBox(
                    width: 2,
                  ),*/
                  Expanded(
                    child: GestureDetector(
                      child: Container(
                        padding: EdgeInsets.all(5),
                        color: Theme.of(context).primaryColor,
                        child: Center(
                          child: Column(
                            children: <Widget>[
                              Icon(
                                Icons.compare,
                                color: Colors.white,
                                size: 20,
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Text(
                                'COMPARE',
                                style: bottomButtonStyle,
                              ),
                            ],
                          ),
                        ),
                      ),
                      onTap: () {
                        Navigator.push(
                          context,
                          PageTransition(
                            type: PageTransitionType.rightToLeft,
                            child: CompareScreen(),
                          ),
                        );
                      },
                    ),
                  ),
                ],
              ),
            ),
    );
  }

  void _sendDataToFilterScreen(BuildContext context) async {
    var result = await Navigator.push(
        context,
        PageTransition(
            type: PageTransitionType.rightToLeft,
            child: FilterScreen(convertDataToJson: convertDataToJson)));
    finalData = [];
    for (int i = 0; i < result.length; i++) {
      if (result[i] == 1) {
        setState(() {
          finalData.add(convertDataToJson['listFilterPojo'][i]);
        });
      }
    }
  }
}
