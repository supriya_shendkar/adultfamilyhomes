import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import '../profile/feedback.dart';

class RateUsPage extends StatefulWidget {
  @override
  _RateUsPageState createState() => _RateUsPageState();
}

class _RateUsPageState extends State<RateUsPage> {
  void feedBackBtn(BuildContext ctx) {
    Navigator.push(
      context,
      PageTransition(
        type: PageTransitionType.rightToLeft,
        child: FeedBackPage(),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: BackButton(
          color: Colors.white,
        ),
        title: Text(
          'RateUs',
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: Container(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Image.asset(
                'Icons/adultfamilyhomes.png',
                fit: BoxFit.contain,
                height: 80,
              ),
              SizedBox(height: 70),
              Text(
                'Enjoying AFH',
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
              ),
              SizedBox(height: 40),
              Text('Please rate your experience'),
              SizedBox(height: 10),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  GestureDetector(
                    onTap: () => feedBackBtn(context),
                    child: FlatButton(
                      child: Text('SEND FEEDBACK'),
                      onPressed: () => feedBackBtn(context), // {
                      // print('button pressed');
                      //   },
                    ),
                  ),
                  RaisedButton(
                    color: Theme.of(context).accentColor,
                    child: Text('RATE 5 STAR'),
                    onPressed: () {
                      print('button pressed');
                    },
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
