
import 'package:afh/profile/loginpage.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'RateUs.dart';
import '../profile/feedback.dart';

class HelpCenterPage extends StatefulWidget {
  @override
  _HelpCenterPageState createState() => _HelpCenterPageState();
}

class _HelpCenterPageState extends State<HelpCenterPage> {
  var uid;
  checkStatus() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (prefs.getString('ownername') != null) {
      setState(() {
        uid = prefs.getString('userID');
      });
    }
  }

  @override
  void initState() {
    super.initState();
    this.checkStatus();
  }

  void onTapRateUs(BuildContext context) {
    Navigator.push(
      context,
      PageTransition(
        type: PageTransitionType.rightToLeft,
        child: RateUsPage(),
      ),
    );
  }

  void onTapfeedback(BuildContext context) {
    uid == null
        ? Navigator.push(
            context,
            PageTransition(
              type: PageTransitionType.rightToLeft,
              child: LoginPage(),
            ),
          )
        : Navigator.push(
            context,
            PageTransition(
              type: PageTransitionType.rightToLeft,
              child: FeedBackPage(),
            ),
          );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: BackButton(
          color: Colors.white,
        ),
        title: Text(
          'Help Center',
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: Container(
        child: Column(
          children: <Widget>[
            GestureDetector(
              onTap: () => onTapfeedback(context),
              child: Container(
                color: Colors.white,
                child: ListTile(
                  leading: Icon(
                    Icons.rate_review,
                    color: Theme.of(context).primaryColorDark,
                  ),
                  title: Text('Contact us'),
                ),
              ),
            ),
            SizedBox(
              height: 5,
            ),
            GestureDetector(
              onTap: () => onTapRateUs(context),
              child: Container(
                color: Colors.white,
                child: ListTile(
                  leading: Icon(Icons.star,
                      color: Theme.of(context).primaryColorDark),
                  title: Text('Rate Us'),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
