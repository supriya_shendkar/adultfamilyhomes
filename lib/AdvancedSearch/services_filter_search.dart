import 'package:flutter/material.dart';
import 'package:grouped_buttons/grouped_buttons.dart';

class ServicesFilterSearch extends StatefulWidget {
  @override
  _ServicesFilterSearchState createState() => _ServicesFilterSearchState();
}

class _ServicesFilterSearchState extends State<ServicesFilterSearch> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: BackButton(
          color: Colors.white,
        ),
        title: Text(
          'Services',
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: SafeArea(
        child: Container(
          color: Colors.white,
          child: SingleChildScrollView(
            child: CheckboxGroup(
              labels: <String>[
                "24 hour care",
                "Dementia/"
                    "Alzheimer's",
                "Diabetes",
                "Hospice",
                "Wound Care",
                "AwakeNight Staff",
                "Asst.with Daily Living",
                "Medication Delivery",
                "Massage Therapies",
                "Parkinson's",
                "Developmental Disabilities",
                "PT/OT & Sp.Therapy",
                "Respite CareShort-term",
                "Emergency call buttons",
                "Medication Management",
                "Pet Care",
                "RN Operated",
                "Oxygen Therapy",
                "Visual/Hearing Impaired",
                "Stroke",
                "Home Doctor",
                "Housekeeping "
                    "Laundry",
                "Podiatrist",
              ],
              onSelected: (List<String> checked) => print(
                checked.toString(),
              ),
            ),
          ),
        ),
      ),
      bottomNavigationBar: Container(
        height: 40,
        color: Theme.of(context).primaryColor,
        child: Center(
          child: Text(
            'DONE',
            style: TextStyle(fontSize: 22, color: Colors.white),
          ),
        ),
      ),
    );
  }
}
