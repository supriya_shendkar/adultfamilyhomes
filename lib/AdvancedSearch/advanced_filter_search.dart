
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import '../home_screen/search_by_city_name.dart';
import '../second_page/second_page_screen.dart';
import 'amenities_filter_search.dart';
import 'room_filter_search.dart';
import 'services_filter_search.dart';

class AdvancedFilterSearch extends StatefulWidget {
  @override
  _AdvancedFilterSearchState createState() => _AdvancedFilterSearchState();
}

class _AdvancedFilterSearchState extends State<AdvancedFilterSearch> {
  final List<String> _dropdownValues = [
    "Yes",
    "No",
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Advanced Search',
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: SafeArea(
        child: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Card(
                margin: EdgeInsets.fromLTRB(10, 10, 10, 10),
                color: Theme.of(context).primaryColorDark,
                child: FlatButton(
                  onPressed: () {
                    Navigator.push(
                      context,
                      PageTransition(
                        type: PageTransitionType.rightToLeft,
                        child: SearchBar(),
                      ),
                    );
                  },
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      'Enter city name or zipcode',
                      style: TextStyle(color: Colors.white, fontSize: 16),
                    ),
                  ),
                ),
              ),
              Card(
                margin: EdgeInsets.fromLTRB(10, 10, 10, 10),
                color: Theme.of(context).primaryColorDark,
                child: FlatButton(
                  onPressed: () {
                    Navigator.push(
                      context,
                      PageTransition(
                        type: PageTransitionType.rightToLeft,
                        child: RoomFilterSearch(),
                      ),
                    );
                  },
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      'Room type',
                      style: TextStyle(color: Colors.white, fontSize: 16),
                    ),
                  ),
                ),
              ),
              Card(
                margin: EdgeInsets.fromLTRB(10, 10, 10, 10),
                color: Theme.of(context).primaryColorDark,
                child: FlatButton(
                  onPressed: () {
                    Navigator.push(
                      context,
                      PageTransition(
                        type: PageTransitionType.rightToLeft,
                        child: ServicesFilterSearch(),
                      ),
                    );
                  },
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      'Services',
                      style: TextStyle(color: Colors.white, fontSize: 16),
                    ),
                  ),
                ),
              ),
              Card(
                margin: EdgeInsets.fromLTRB(10, 10, 10, 10),
                color: Theme.of(context).primaryColorDark,
                child: FlatButton(
                  onPressed: () {
                    Navigator.push(
                      context,
                      PageTransition(
                        type: PageTransitionType.rightToLeft,
                        child: AmenitiesFilterSearch(),
                      ),
                    );
                  },
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      'Amenities and activities',
                      style: TextStyle(color: Colors.white, fontSize: 16),
                    ),
                  ),
                ),
              ),
              Card(
                color: Theme.of(context).primaryColorDark,
                margin: EdgeInsets.fromLTRB(10, 10, 10, 10),
                child: DropdownButton(
                  iconEnabledColor: Colors.white,
                  iconSize: 40,
                  elevation: 50,
                  underline: Container(
                    color: Color(0xFF41B3A3),
                  ),
                  items: _dropdownValues
                      .map((value) => DropdownMenuItem(
                            child: Text(value),
                            value: value,
                          ))
                      .toList(),
                  onChanged: (String value) {},
                  isExpanded: false,
                  hint: Text(
                    '  Medicaid                                                    ',
                    style: TextStyle(color: Colors.white),
                  ),
                ),
              ),
              Container(
                height: 60,
                padding: EdgeInsets.all(10),
                child: FlatButton(
                  color: Theme.of(context).accentColor,
                  onPressed: () {
                    Navigator.push(
                      context,
                      PageTransition(
                        type: PageTransitionType.rightToLeft,
                        child: SecondPageScreen(),
                      ),
                    );
                  },
                  child: Text(
                    'SEARCH',
                    style: TextStyle(fontSize: 18, color: Colors.white),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
