import 'package:flutter/material.dart';
import 'package:grouped_buttons/grouped_buttons.dart';

class AmenitiesFilterSearch extends StatefulWidget {
  @override
  _AmenitiesFilterSearchState createState() => _AmenitiesFilterSearchState();
}

class _AmenitiesFilterSearchState extends State<AmenitiesFilterSearch> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: BackButton(
          color: Colors.white,
        ),
        title: Text(
          'Amenities and Activities',
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: SafeArea(
        child: Container(
          color: Colors.white,
          child: SingleChildScrollView(
            child: CheckboxGroup(
              labels: <String>[
                "Activities coordinator",
                "Games",
                "Library on Wheels",
                "Cable TV",
                "Telephone",
                "Arts and Crafts",
                "Birthday celebration",
                "Wheelchair accessible",
                "Beautician",
                "Newspaper",
                "Music",
                "Scenic Drive",
                "Generator",
                "Internet access",
              ],
              onSelected: (List<String> checked) => print(
                checked.toString(),
              ),
            ),
          ),
        ),
      ),
      bottomNavigationBar: Container(
        height: 40,
        color: Theme.of(context).primaryColor,
        child: Center(
          child: Text(
            'DONE',
            style: TextStyle(fontSize: 22, color: Colors.white),
          ),
        ),
      ),
    );
  }
}
