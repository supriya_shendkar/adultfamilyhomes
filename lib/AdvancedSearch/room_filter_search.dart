import 'package:flutter/material.dart';
import 'package:grouped_buttons/grouped_buttons.dart';

class RoomFilterSearch extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: BackButton(
          color: Colors.white,
        ),
        title: Text(
          'Room Types',
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: SafeArea(
        child: Container(
          color: Colors.white,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                child: CheckboxGroup(
                  labels: <String>[
                    "Private Room",
                    "Semi Private Room",
                    "Room & Bath",
                    "Room & Shower"
                  ],
                  onSelected: (List<String> checked) => print(
                    checked.toString(),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
      bottomNavigationBar: Container(
        height: 40,
        color: Theme.of(context).primaryColor,
        child: Center(
            child: Text(
          'DONE',
          style: TextStyle(fontSize: 22, color: Colors.white),
        )),
      ),
    );
  }
}
