import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'splash_screen.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
  ]);
  runApp(MyApp());
}


class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primaryColor: const Color(0xFF394D67),
          accentColor: const Color(0xFF97C160),
          primaryColorDark: Color(0xFF538DB8),
          canvasColor: Colors.grey[200],
          //fontFamily: 'OpenSans-Regular',
        ),
        initialRoute: '/',
        routes: {
          '/': (context) => SplashScreen(),
        });
  }
}
