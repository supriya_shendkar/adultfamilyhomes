class ProfileUpdate {
  final String ownerName;
  final String ownerEmail;
  final String ownerMobile;
  final String ownerCity;
  final String ownerAddress;
  final String currentPassword;
  final String bio;

  ProfileUpdate({
    this.ownerName,
    this.ownerEmail,
    this.ownerMobile,
    this.ownerCity,
    this.ownerAddress,
    this.currentPassword,
    this.bio,
  });

  factory ProfileUpdate.fromJson(Map<String, dynamic> json) {
    return ProfileUpdate(
      ownerName: json['ownerName'],
      ownerEmail: json['ownerEmail'],
      ownerMobile: json['ownerMobile'],
      ownerCity: json['ownerCity'],
      ownerAddress: json['ownerAddress'],
      currentPassword: json['currentPassword'],
      bio: json['bio'],
    );
  }
  Map toMap() {
    var map = new Map<String, dynamic>();
    map["ownerName"] = ownerName;
    map["ownerEmail"] = ownerEmail;
    map["ownerMobile"] = ownerMobile;
    map["ownerCity"] = ownerCity;
    map["ownerAddress"] = ownerAddress;
    map["currentPassword"] = currentPassword;
    map["bio"] = bio;
    return map;
  }
}
