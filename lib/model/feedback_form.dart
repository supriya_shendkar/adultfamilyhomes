class FeedBackForm {
  final String name;
  final String email;
  final String phone;
  final String subject;
  final String message;

  FeedBackForm({
    this.name,
    this.email,
    this.phone,
    this.subject,
    this.message,
  });

  factory FeedBackForm.fromJson(Map<String, dynamic> json) {
    return FeedBackForm(
      name: json['name'],
      phone: json['phone'],
      email: json['email'],
      subject: json['subject'],
      message: json['message'],
    );
  }
  Map toMap() {
    var map = new Map<String, dynamic>();
    map["name"] = name;
    map["phone"] = phone;
    map["email"] = email;
    map["subject"] = subject;
    map["message"] = message;
    return map;
  }

  @override
  String toString() {
    return 'FeedBackForm{name: $name, email: $email, phone: $phone, subject: $subject, message: $message}';
  }
}
