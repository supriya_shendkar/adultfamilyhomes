class SendMessageToOwner {
  final String message;
  final String ownerId;
  final String stayId;

  SendMessageToOwner({
    this.message,
    this.ownerId,
    this.stayId,
  });

  factory SendMessageToOwner.fromJson(Map<String, dynamic> json) {
    return SendMessageToOwner(
      message: json['message'],
      ownerId: json['ownerId'],
      stayId: json['stayId'],
    );
  }
  Map toMap() {
    var map = new Map<String, dynamic>();
    map["message"] = message;
    map["ownerId"] = ownerId;
    map["stayId"] = stayId;

    return map;
  }

  @override
  String toString() {
    return 'SendMessageToOwner{message: $message, ownerId: $ownerId, stayId: $stayId}';
  }
}
