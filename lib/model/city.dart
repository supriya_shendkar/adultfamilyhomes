class City {
  String userCity;

  City({
    this.userCity,
  });

  factory City.fromJson(Map<String, dynamic> parsedJson) {
    return City(
      userCity: parsedJson['userCity'] as String,
    );
  }
}
