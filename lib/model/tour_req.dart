class SaveTourRequest {
  final String name;
  final String mobile;
  final String email;
  final String stayId;
  final String homeName;
  final String userId;
  final String dateSlot;
  final String timeSlot;

  SaveTourRequest(
      {this.name,
      this.mobile,
      this.email,
      this.stayId,
      this.homeName,
      this.userId,
      this.dateSlot,
      this.timeSlot});

  factory SaveTourRequest.fromJson(Map<String, dynamic> json) {
    return SaveTourRequest(
      name: json['name'],
      mobile: json['mobile'],
      email: json['email'],
      stayId: json['stayId'],
      homeName: json['homeName'],
      userId: json['userId'],
      dateSlot: json['dateSlot'],
      timeSlot: json['timeSlot'],
    );
  }
  Map toMap() {
    var map = new Map<String, dynamic>();
    map["name"] = name;
    map["mobile"] = mobile;
    map["email"] = email;
    map["stayId"] = stayId;
    map["homeName"] = homeName;
    //map["ownerEmail"] = ownerEmail;
    map["userId"] = userId;
    map["dateSlot"] = dateSlot;
    map["timeSlot"] = timeSlot;
    return map;
  }

  @override
  String toString() {
    return 'SaveTourRequest{name: $name, mobile: $mobile, email: $email, stayId: $stayId, homeName: $homeName, userId: $userId, dateSlot: $dateSlot, timeSlot: $timeSlot}';
  }
}
