class CallBack {
  final String callUser;
  final String callPhone;
  final String callEmail;
  final String stayId;
  final String homeName;
  final String ownerId;
  final String requestDate;
  final String slugTitle;

  CallBack(
      {this.callUser,
      this.callPhone,
      this.callEmail,
      this.stayId,
      this.homeName,
      this.ownerId,
      this.requestDate,
      this.slugTitle});

  factory CallBack.fromJson(Map<String, dynamic> json) {
    return CallBack(
      callUser: json['callUser'],
      callPhone: json['callPhone'],
      callEmail: json['callEmail'],
      stayId: json['stayId'],
      homeName: json['homeName'],
      ownerId: json['ownerId'],
      requestDate: json['requestDate'],
      slugTitle: json['slugTitle'],
    );
  }
  Map toMap() {
    var map = new Map<String, dynamic>();
    map["callUser"] = callUser;
    map["callPhone"] = callPhone;
    map["callEmail"] = callEmail;
    map["stayId"] = stayId;
    map["homeName"] = homeName;
    map["ownerId"] = ownerId;
    map["requestDate"] = requestDate;
    map["slugTitle"] = slugTitle;
    return map;
  }

  @override
  String toString() {
    return 'CallBack{callUser: $callUser, callPhone: $callPhone, callEmail: $callEmail, stayId: $stayId, homeName: $homeName, ownerId: $ownerId, requestDate: $requestDate,}';
  }
}
