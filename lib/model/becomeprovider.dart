class BecomeProvider {
  final String firstName;
  final String lastName;
  final String email;
  final String mobNo;
  final String medicalBackground;
  final String interestedIn;
  final String queOrComment;
  final String primaryPurpose;
  final String goals;
  final String planDuration;

  BecomeProvider(
      {this.firstName,
      this.lastName,
      this.email,
      this.mobNo,
      this.medicalBackground,
      this.interestedIn,
      this.queOrComment,
      this.primaryPurpose,
      this.goals,
      this.planDuration});

  factory BecomeProvider.fromJson(Map<String, dynamic> json) {
    return BecomeProvider(
      firstName: json['firstName'],
      lastName: json['lastName'],
      email: json['email'],
      mobNo: json['mobNo'],
      medicalBackground: json['medicalBackground'],
      interestedIn: json['interestedIn'],
      queOrComment: json['queOrComment'],
      primaryPurpose: json['primaryPurpose'],
      goals: json['goals'],
      planDuration: json['planDuration'],
    );
  }
  Map toMap() {
    var map = new Map<String, dynamic>();
    map["firstName"] = firstName;
    map["lastName"] = lastName;
    map["email"] = email;
    map["mobNo"] = mobNo;
    map["medicalBackground"] = medicalBackground;
    map["interestedIn"] = interestedIn;
    map["queOrComment"] = queOrComment;
    map["primaryPurpose"] = primaryPurpose;
    map["goals"] = goals;
    map["planDuration"] = planDuration;
    return map;
  }
}
