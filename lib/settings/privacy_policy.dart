import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import '../constant.dart';
import 'package:flutter_html/flutter_html.dart';

class PrivacyPolicy extends StatefulWidget {
  @override
  _PrivacyPolicyState createState() => _PrivacyPolicyState();
}

class _PrivacyPolicyState extends State<PrivacyPolicy> {
  var html;
  var response;
  Future<String> privacyPolicy() async {
    response = await http.get(Uri.encodeFull(BASE_URL + "/afh/privacypolicy"),
        headers: {"Accept": "application/json"});
    if (this.mounted) {
      setState(() {
        var privacyPolicyData = json.decode(response.body);
        html = privacyPolicyData['privacy'];
      });
    }
    return "Success";
  }

  @override
  void initState() {
    super.initState();
    this.privacyPolicy();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Privacy Policy')),
      body: response == null
          ? Center(
              child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                CircularProgressIndicator(),
                SizedBox(height: 30),
                Text('    Loading . . .'),
              ],
            ))
          : SingleChildScrollView(
              child: Padding(
                padding: EdgeInsets.all(10),
                child: Column(
                  children: <Widget>[new Html(data: html)],
                ),
              ),
            ),
    );
  }
}
