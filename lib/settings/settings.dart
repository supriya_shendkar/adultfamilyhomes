import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'cancelation_policy.dart';
import 'privacy_policy.dart';
import 'terms_and_conditions.dart';

class Setting extends StatefulWidget {
  @override
  _SettingState createState() => _SettingState();
}

class _SettingState extends State<Setting> {
  String uid;
  @override
  void initState() {
    super.initState();
    checkStatus();
  }

  checkStatus() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (prefs.getString('ownername') != null) {
      setState(() {
        uid = prefs.getString('userID');
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: BackButton(
          color: Colors.white,
        ),
        title: Text(
          'Settings',
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: Column(
        children: <Widget>[
          GestureDetector(
            onTap: () {
              Navigator.push(
                  context,
                  PageTransition(
                      type: PageTransitionType.rightToLeft,
                      child: PrivacyPolicy()));
            },
            child: Card(
              child: ListTile(
                title: Text('Privacy Policy'),
              ),
            ),
          ),
          GestureDetector(
            onTap: () {
              Navigator.push(
                  context,
                  PageTransition(
                      type: PageTransitionType.rightToLeft,
                      child: CancellationPolicy()));
            },
            child: Card(
              child: ListTile(
                title: Text('Cancellation Policy'),
              ),
            ),
          ),
          GestureDetector(
            onTap: () {
              Navigator.push(
                  context,
                  PageTransition(
                      type: PageTransitionType.rightToLeft,
                      child: TermsAndConditions()));
            },
            child: Card(
              child: ListTile(
                title: Text('Terms and Conditions'),
              ),
            ),
          ),
          Card(
            child: ListTile(
              title: Text('Application Version'),
              subtitle: Text('1.0.1'),
            ),
          ),
          uid == null
              ? Container()
              : GestureDetector(
                  onTap: () async {
                    SharedPreferences prefs =
                        await SharedPreferences.getInstance();
                    prefs.remove('token');
                    prefs.remove('ownername');
                    prefs.remove('userID');
                    Navigator.pop(context);
                  },
                  child: Card(
                    child: ListTile(
                      trailing: Icon(
                        Icons.exit_to_app,
                        color: Theme.of(context).primaryColorDark,
                      ),
                      title: Text('Logout'),
                    ),
                  ),
                ),
        ],
      ),
    );
  }
}
