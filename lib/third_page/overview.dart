import 'package:afh/home_screen/similar_homes.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:page_transition/page_transition.dart';
import 'activities.dart';
import 'bottom_navigation_bar_buttons.dart';
import 'services.dart';

class OverView extends StatefulWidget {
  List result1 = List();
  List result2 = List();
  List result3 = List();
  List images = List();
  final String pgName,
      address,
      medicAid,
      imagePath,
      cityName,
      ownerName,
      webURL,
      zip,
      ownerEmail,
      shopCentDist,
      hospitalDist,
      busStatDist,
      airportDist,
      coffeeShopDist,
      stayId,
      ownerId,
      slugTitle;

  OverView(
      {Key key,
      @required this.stayId,
      @required this.result1,
      @required this.images,
      @required this.medicAid,
      @required this.pgName,
      @required this.address,
      @required this.imagePath,
      @required this.result2,
      @required this.cityName,
      @required this.ownerName,
      @required this.webURL,
      @required this.result3,
      @required this.zip,
      @required this.ownerEmail,
      @required this.shopCentDist,
      @required this.hospitalDist,
      @required this.busStatDist,
      @required this.airportDist,
      @required this.coffeeShopDist,
      @required this.ownerId,
      @required this.slugTitle})
      : super(key: key);
  @override
  _OverViewState createState() => _OverViewState();
}

class _OverViewState extends State<OverView> {
  var amenities = [];
  CarouselSlider carouselSlider;
  int _current = 0;

  List<T> map<T>(List list, Function handler) {
    List<T> result = [];
    for (var i = 0; i < list.length; i++) {
      result.add(handler(i, list[i]));
    }
    return result;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      body: Center(
        child: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Container(
                color: Colors.white,
                child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: <Widget>[
                          carouselSlider = CarouselSlider(
                            height: 300.0,
                            initialPage: 0,
                            enlargeCenterPage: true,
                            autoPlay: false,
                            reverse: false,
                            enableInfiniteScroll: true,
                            scrollDirection: Axis.horizontal,
                            onPageChanged: (index) {
                              setState(() {
                                _current = index;
                              });
                            },
                            items: widget.images.map((images) {
                              return Builder(
                                builder: (BuildContext context) {
                                  return Card(
                                    elevation: 10,
                                    child: Container(
                                      width: MediaQuery.of(context).size.width,
                                      decoration: BoxDecoration(
                                        color: Color(0xFF41B3A3),
                                      ),
                                      child: Image.network(
                                        "" + images['imagePath'],
                                        fit: BoxFit.fill,
                                      ),
                                    ),
                                  );
                                },
                              );
                            }).toList(),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: map<Widget>(widget.images, (index, url) {
                              return Container(
                                width: 10.0,
                                height: 10.0,
                                margin: EdgeInsets.symmetric(
                                    vertical: 10.0, horizontal: 2.0),
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: _current == index
                                      ? Theme.of(context).primaryColor
                                      : Theme.of(context).primaryColorDark,
                                ),
                              );
                            }),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Expanded(
                            child: Container(
                              padding: EdgeInsets.only(left: 10),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(widget.pgName,
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 16)),
                                  SizedBox(height: 5),
                                  Text(widget.address,
                                      style: TextStyle(
                                          color: Colors.grey, fontSize: 13)),
                                  SizedBox(height: 10),
                                  Row(
                                    children: <Widget>[
                                      Text(
                                        'Owner/Provider: ',
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold),
                                      ),
                                      Text(
                                        widget.ownerName,
                                      ),
                                    ],
                                  ),
                                  SizedBox(height: 10),
                                  Row(
                                    children: <Widget>[
                                      widget.webURL == ""
                                          ? Container()
                                          : Icon(
                                              FontAwesomeIcons.globe,
                                              color: Theme.of(context)
                                                  .primaryColorDark,
                                              size: 15,
                                            ),
                                      SizedBox(
                                        width: 5,
                                      ),
                                      Expanded(
                                        child: Text(
                                          widget.webURL,
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .primaryColorDark,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(right: 5),
                            child: Column(
                              children: <Widget>[
                                Row(
                                  children: <Widget>[
                                    widget.medicAid == 'yes'
                                        ? new Image.asset('Icons/medicAid.png',
                                            scale: 1.3)
                                        : Container(),
                                    SizedBox(width: 5),
                                    widget.medicAid == 'yes'
                                        ? new Text(
                                            'Provides\nMedicAid',
                                            style: TextStyle(
                                                color: Theme.of(context)
                                                    .primaryColorDark,
                                                fontSize: 13),
                                          )
                                        : Container(),
                                  ],
                                ),
                                SizedBox(
                                  height: 25,
                                ),
                                FlatButton(
                                  onPressed: () {},
                                  child: Text(
                                    'VIEW MAP',
                                    style: TextStyle(
                                        color: Theme.of(context).accentColor,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                padding:
                    EdgeInsets.only(left: 10, top: 10, right: 0, bottom: 10),
                color: Colors.white,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      'Rooms Available',
                      style: TextStyle(
                        fontSize: 20,
                        // fontWeight: FontWeight.bold,
                        color: Colors.black,
                      ),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    /*Container(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          for (var item in widget.result3)
                            Text(
                              item,
                              style: TextStyle(
                                  color: Theme.of(context).primaryColorDark,
                                  fontSize: 15),
                            ),
                        ],
                      ),
                    ),*/
                  ],
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                height: 50,
                //color: Colors.white,
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: GestureDetector(
                        onTap: () {
                          Navigator.push(
                            context,
                            PageTransition(
                                type: PageTransitionType.rightToLeft,
                                child: Services(
                                  result1: widget.result1,
                                )),
                          );
                        },
                        child: Card(
                          child: Center(
                            child: Padding(
                              padding: EdgeInsets.only(left: 10),
                              child: Row(
                                children: <Widget>[
                                  Text(
                                    'Services',
                                    style: TextStyle(
                                      fontSize: 20,
                                      // fontWeight: FontWeight.bold,
                                      color: Colors.black,
                                    ),
                                  ),
                                  SizedBox(
                                    width: 50,
                                  ),
                                  Icon(
                                    Icons.arrow_forward_ios,
                                    size: 16,
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      child: GestureDetector(
                        onTap: () {
                          Navigator.push(
                            context,
                            PageTransition(
                                type: PageTransitionType.rightToLeft,
                                child: Activities(
                                  result2: widget.result2,
                                )),
                          );
                        },
                        child: Card(
                          child: Center(
                            child: Padding(
                              padding: EdgeInsets.only(left: 10),
                              child: Row(
                                children: <Widget>[
                                  Text(
                                    'Activities',
                                    style: TextStyle(
                                      fontSize: 20,
                                      //fontWeight: FontWeight.bold,
                                      color: Colors.black,
                                    ),
                                  ),
                                  SizedBox(
                                    width: 50,
                                  ),
                                  Icon(
                                    Icons.arrow_forward_ios,
                                    size: 16,
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                padding:
                    EdgeInsets.only(left: 10, top: 10, right: 0, bottom: 10),
                color: Colors.white,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "Where Are You Located?",
                      style: TextStyle(
                        fontSize: 20,
                        //fontWeight: FontWeight.bold,
                        color: Colors.black,
                      ),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: Row(
                            children: <Widget>[
                              Text(
                                'Country:  ',
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              Text(
                                'USA',
                                style: TextStyle(
                                    color: Theme.of(context).primaryColorDark),
                              ),
                            ],
                          ),
                        ),
                        Expanded(
                          child: Row(
                            children: <Widget>[
                              Text(
                                'City: ',
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              Text(widget.cityName,
                                  style: TextStyle(
                                      color:
                                          Theme.of(context).primaryColorDark)),
                            ],
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: Row(
                            children: <Widget>[
                              Text(
                                'Zip Code:  ',
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              Text(
                                widget.zip,
                                style: TextStyle(
                                    color: Theme.of(context).primaryColorDark),
                              ),
                            ],
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                padding:
                    EdgeInsets.only(left: 10, top: 10, right: 0, bottom: 10),
                color: Colors.white,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "Neighborhood",
                      style: TextStyle(
                        fontSize: 20,
                        //fontWeight: FontWeight.bold,
                        color: Colors.black,
                      ),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: Row(
                            children: <Widget>[
                              Text(
                                'Shopping Center:  ',
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              widget.shopCentDist == null
                                  ? Text(
                                      'No Data',
                                      style: TextStyle(
                                          color: Theme.of(context)
                                              .primaryColorDark),
                                    )
                                  : Text(widget.shopCentDist,
                                      style: TextStyle(
                                          color: Theme.of(context)
                                              .primaryColorDark)),
                            ],
                          ),
                        ),
                        Expanded(
                          child: Row(
                            children: <Widget>[
                              Text(
                                'Hospital: ',
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              widget.hospitalDist == null
                                  ? Text(
                                      'No Data',
                                      style: TextStyle(
                                          color: Theme.of(context)
                                              .primaryColorDark),
                                    )
                                  : Text(widget.hospitalDist,
                                      style: TextStyle(
                                          color: Theme.of(context)
                                              .primaryColorDark)),
                            ],
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: Row(
                            children: <Widget>[
                              Text(
                                'Bus Station:  ',
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              widget.busStatDist == null
                                  ? Text(
                                      'No Data',
                                      style: TextStyle(
                                          color: Theme.of(context)
                                              .primaryColorDark),
                                    )
                                  : Text(widget.busStatDist,
                                      style: TextStyle(
                                          color: Theme.of(context)
                                              .primaryColorDark)),
                            ],
                          ),
                        ),
                        Expanded(
                          child: Row(
                            children: <Widget>[
                              Text(
                                'Airport: ',
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              widget.airportDist == null
                                  ? Text(
                                      'No Data',
                                      style: TextStyle(
                                          color: Theme.of(context)
                                              .primaryColorDark),
                                    )
                                  : Text(widget.airportDist,
                                      style: TextStyle(
                                          color: Theme.of(context)
                                              .primaryColorDark)),
                            ],
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: Row(
                            children: <Widget>[
                              Text(
                                'Coffee Shop:  ',
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              widget.coffeeShopDist == null
                                  ? Text(
                                      'No Data',
                                      style: TextStyle(
                                          color: Theme.of(context)
                                              .primaryColorDark),
                                    )
                                  : Text(widget.coffeeShopDist,
                                      style: TextStyle(
                                          color: Theme.of(context)
                                              .primaryColorDark)),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                color: Colors.white,
                child: SimilarHomes(
                  cityName: widget.cityName,
                ),
              ),
            ],
          ),
        ),
      ),
      bottomNavigationBar: Container(
        height: 50,
        color: Colors.white,
        child: BottomNavigationBarButtons(
          pgName: widget.pgName,
          ownerEmail: widget.ownerEmail,
          stayId: widget.stayId,
          ownerId: widget.ownerId,
          slugTitle: widget.slugTitle,
        ),
      ),
    );
  }
}
