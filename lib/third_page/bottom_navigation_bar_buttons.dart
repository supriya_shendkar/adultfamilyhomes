import 'dart:convert';
import 'package:afh/model/send_message_to_owner.dart';
import 'package:afh/profile/loginpage.dart';
import 'package:afh/third_page/reserve-room.dart';
import 'package:afh/third_page/tour_request.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../constant.dart';
import 'call_back_request.dart';
import 'package:http/http.dart' as http;

class BottomNavigationBarButtons extends StatefulWidget {
  final String stayId, pgName, ownerEmail, ownerId, slugTitle;
  BottomNavigationBarButtons(
      {Key key,
      @required this.pgName,
      @required this.ownerEmail,
      @required this.stayId,
      @required this.ownerId,
      @required this.slugTitle})
      : super(key: key);
  @override
  _BottomNavigationBarButtonsState createState() =>
      _BottomNavigationBarButtonsState();
}

class _BottomNavigationBarButtonsState
    extends State<BottomNavigationBarButtons> {
  String name;
  String uid;

  @override
  void initState() {
    super.initState();
    checkStatus();
  }

  checkStatus() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (prefs.getString('ownername') != null) {
      if(this.mounted){
      setState(() {
        name = prefs.getString('ownername');
        uid = prefs.getString('userID');
      });
    }}
  }

  TextEditingController messageController = TextEditingController();
  var _formKey = GlobalKey<FormState>();
  var statusCode;
  final saveTourRequestURL = BASE_URL + "/api/myafh/owner/createusermsg";
  Future<SendMessageToOwner> messageToOwner(String url, {map}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return http.post(url, body: utf8.encode(json.encode(map)), headers: {
      "content-type": "application/json",
      "Accept": "application/json",
      "access-control-allow-origin": "*",
      "Authorization": "Bearer " + prefs.getString("token"),
    }).then((http.Response response) {
      statusCode = response.statusCode;
      return SendMessageToOwner.fromJson(json.decode(response.body));
    });
  }

  _showDialog() async {
    await showDialog(
        context: context,
        child: new AlertDialog(
          contentPadding: const EdgeInsets.all(16.0),
          content: Row(
            children: <Widget>[
              new Expanded(
                child: Form(
                  key: _formKey,
                  child: new TextFormField(
                    autofocus: true,
                    decoration: new InputDecoration(
                        border: OutlineInputBorder(),
                        hintMaxLines: 5,
                        labelText: 'Enter message',
                        hintText: 'Please type here'),
                    maxLines: 5,
                    keyboardType: TextInputType.text,
                    controller: messageController,
                    validator: (String value) {
                      if (value.isEmpty) {
                        return 'Please enter something here';
                      } else {
                        return null;
                      }
                    },
                    onSaved: (String value) {
                      messageController.text = value;
                    },
                  ),
                ),
              )
            ],
          ),
          actions: <Widget>[
            new FlatButton(
                child: const Text('CANCEL'),
                onPressed: () {
                  Navigator.pop(context);
                }),
            new FlatButton(
                child: const Text('SEND'),
                onPressed: () {
                  if (_formKey.currentState.validate()) {
                    return sendMessage();
                  }
                  if (statusCode == 200) {
                    _showFinalDialog();
                  }
                }),
          ],
        ));
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        name != null
            ? Expanded(
                child: GestureDetector(
                  onTap: () async {
                    await _showDialog();
                  },
                  child: Container(
                    color: Theme.of(context).primaryColor,
                    padding: EdgeInsets.all(2),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Icon(
                          Icons.message,
                          color: Colors.white,
                          size: 20,
                        ),
                        SizedBox(height: 5),
                        Text('Message', style: bottomButtonStyle),
                      ],
                    ),
                  ),
                ),
              )
            : Expanded(
                child: GestureDetector(
                  onTap: () async {
                    Navigator.push(
                      context,
                      PageTransition(
                        type: PageTransitionType.rightToLeft,
                        child: LoginPage(),
                      ),
                    );
                  },
                  child: Container(
                    color: Theme.of(context).primaryColor,
                    padding: EdgeInsets.all(2),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Icon(
                          Icons.message,
                          color: Colors.white,
                          size: 20,
                        ),
                        SizedBox(height: 5),
                        Text('Message', style: bottomButtonStyle),
                      ],
                    ),
                  ),
                ),
              ),
        SizedBox(
          width: 2,
        ),
        name != null
            ? Expanded(
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                      context,
                      PageTransition(
                        type: PageTransitionType.rightToLeft,
                        child: TourRequest(
                          stayId: widget.stayId,
                          ownerEmail: widget.ownerEmail,
                          pgName: widget.pgName,
                          ownerId: widget.ownerId,
                        ),
                      ),
                    );
                  },
                  child: Container(
                    color: Theme.of(context).primaryColor,
                    padding: EdgeInsets.all(2),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Icon(
                          Icons.directions_car,
                          color: Colors.white,
                          size: 20,
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Text('Tour Home', style: bottomButtonStyle),
                      ],
                    ),
                  ),
                ),
              )
            : Expanded(
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                      context,
                      PageTransition(
                        type: PageTransitionType.rightToLeft,
                        child: LoginPage(),
                      ),
                    );
                  },
                  child: Container(
                    color: Theme.of(context).primaryColor,
                    padding: EdgeInsets.all(2),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Icon(
                          Icons.directions_car,
                          color: Colors.white,
                          size: 20,
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Text('Tour Home', style: bottomButtonStyle),
                      ],
                    ),
                  ),
                ),
              ),
        SizedBox(
          width: 2,
        ),
        name != null
            ? Expanded(
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                      context,
                      PageTransition(
                        type: PageTransitionType.rightToLeft,
                        child: CallBackRequest(
                          stayId: widget.stayId,
                          ownerEmail: widget.ownerEmail,
                          pgName: widget.pgName,
                          ownerId: widget.ownerId,
                        ),
                      ),
                    );
                  },
                  child: Container(
                    color: Theme.of(context).primaryColor,
                    padding: EdgeInsets.all(2),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Icon(
                          Icons.call,
                          color: Colors.white,
                          size: 20,
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Text('CallBack', style: bottomButtonStyle),
                      ],
                    ),
                  ),
                ),
              )
            : Expanded(
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                      context,
                      PageTransition(
                        type: PageTransitionType.rightToLeft,
                        child: LoginPage(),
                      ),
                    );
                  },
                  child: Container(
                    color: Theme.of(context).primaryColor,
                    padding: EdgeInsets.all(2),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Icon(
                          Icons.call,
                          color: Colors.white,
                          size: 20,
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Text('CallBack', style: bottomButtonStyle),
                      ],
                    ),
                  ),
                ),
              ),
        SizedBox(
          width: 2,
        ),
        name != null
            ? Expanded(
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                      context,
                      PageTransition(
                        type: PageTransitionType.rightToLeft,
                        child: ReserveRoom(),
                      ),
                    );
                  },
                  child: Container(
                    color: Theme.of(context).primaryColor,
                    padding: EdgeInsets.all(2),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Icon(
                          Icons.home,
                          color: Colors.white,
                          size: 20,
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Text('Reserve Room', style: bottomButtonStyle),
                      ],
                    ),
                  ),
                ),
              )
            : Expanded(
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                      context,
                      PageTransition(
                        type: PageTransitionType.rightToLeft,
                        child: LoginPage(),
                      ),
                    );
                  },
                  child: Container(
                    color: Theme.of(context).primaryColor,
                    padding: EdgeInsets.all(2),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Icon(
                          Icons.home,
                          color: Colors.white,
                          size: 20,
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Text('Reserve Room', style: bottomButtonStyle),
                      ],
                    ),
                  ),
                ),
              )
      ],
    );
  }

  void sendMessage() async {
    SendMessageToOwner sendMessageToOwner = new SendMessageToOwner(
      message: messageController.text,
      ownerId: widget.ownerId,
      stayId: widget.stayId,
    );
    await messageToOwner(saveTourRequestURL, map: sendMessageToOwner.toMap());
  }

  Future<void> _showFinalDialog() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Tour request sent successfully'),
          actions: <Widget>[
            FlatButton(
              child: Text(
                'Okay',
                style: TextStyle(color: Colors.white),
              ),
              color: Theme.of(context).accentColor,
              onPressed: () {
                Navigator.of(context).popUntil((route) => route.isFirst);
              },
            ),
          ],
        );
      },
    );
  }
}
