import 'package:flutter/material.dart';
import 'bottom_navigation_bar_buttons.dart';

class AboutThisHouse extends StatefulWidget {
  final String description,
      imagePath,
      pgName,
      ownerEmail,
      stayId,
      ownerId,
      slugTitle;

  AboutThisHouse({
    Key key,
    @required this.description,
    @required this.imagePath,
    @required this.pgName,
    @required this.ownerEmail,
    @required this.stayId,
    @required this.ownerId,
    @required this.slugTitle,
  }) : super(key: key);
  @override
  _AboutThisHouseState createState() => _AboutThisHouseState();
}

class _AboutThisHouseState extends State<AboutThisHouse> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Container(
              color: Colors.grey[150],
              padding: EdgeInsets.all(10),
              child: Column(
                children: <Widget>[
                  ClipRRect(
                    borderRadius: BorderRadius.all(Radius.circular(4)),
                    child: new Image.network(widget.imagePath,fit: BoxFit.fill,
                      height: 250,
                      width: 340,),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  DescriptionTextWidget(
                    text: widget.description.replaceAll(
                        RegExp(r"<[^>]*>",
                            multiLine: true, caseSensitive: true),
                        ""),
                    // style: TextStyle(fontSize: 16),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
      bottomNavigationBar: Container(
        height: 50,
        color: Colors.white,
        child: BottomNavigationBarButtons(
          pgName: widget.pgName,
          ownerEmail: widget.ownerEmail,
          stayId: widget.stayId,
          ownerId: widget.ownerId,
          slugTitle: widget.slugTitle,
        ),
      ),
    );
  }
}

class DescriptionTextWidget extends StatefulWidget {
  final String text;

  DescriptionTextWidget({@required this.text});

  @override
  _DescriptionTextWidgetState createState() =>
      new _DescriptionTextWidgetState();
}

class _DescriptionTextWidgetState extends State<DescriptionTextWidget> {
  String firstHalf;
  String secondHalf;
  bool flag = true;

  @override
  void initState() {
    super.initState();
    if (widget.text.length > 1000) {
      firstHalf = widget.text.substring(0, 1000);
      secondHalf = widget.text.substring(1000, widget.text.length);
    } else {
      firstHalf = widget.text;
      secondHalf = "";
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Container(
      child: secondHalf.isEmpty
          ? new Text(firstHalf)
          : new Column(
              children: <Widget>[
                new Text(flag ? (firstHalf + "...") : (firstHalf + secondHalf)),
                new InkWell(
                  child: new Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      new Text(
                        flag ? "More" : "Less",
                        style: new TextStyle(color: Colors.blue, fontSize: 16),
                      ),
                    ],
                  ),
                  onTap: () {
                    setState(() {
                      flag = !flag;
                    });
                  },
                ),
              ],
            ),
    );
  }
}
