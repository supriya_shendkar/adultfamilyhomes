import 'dart:convert';
import 'package:afh/model/tour_req.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

import '../constant.dart';


class TourRequest extends StatefulWidget {
  final String stayId, pgName, ownerEmail, ownerId;
  TourRequest(
      {Key key,
      @required this.pgName,
      @required this.ownerEmail,
      @required this.stayId,
      @required this.ownerId})
      : super(key: key);
  @override
  _TourRequestState createState() => _TourRequestState();
}

class _TourRequestState extends State<TourRequest> {
  var _formKey = GlobalKey<FormState>();
  String _date = "Not set";
  String _time = "Not set";
  var statusCode;
  final now = DateTime.now();
  var formatter = new DateFormat('MM/dd/yyyy');
  String formattedDate;
  String _selectedTime;
  List time = [
    '10:30 AM',
    '11:30 AM',
    '12:30 PM',
    '01:30 PM',
    '02:30 PM',
    '03:30 PM',
    '04:00 PM',
    '05:00 PM',
    '05:30 PM',
    '06:30 PM',
    '07:00 PM',
    '08:00 PM',
  ];
  final saveTourRequestURL = BASE_URL + "/api/myafh/owner/bookhome";
  TextEditingController nameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController mobNoController = TextEditingController();

  Future<SaveTourRequest> tour(String url, {map}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return http.post(url, body: utf8.encode(json.encode(map)), headers: {
      "content-type": "application/json",
      "Accept": "application/json",
      "access-control-allow-origin": "*",
      "Authorization": "Bearer " + prefs.getString("token"),
    }).then((http.Response response) {
      statusCode = response.statusCode;
      return SaveTourRequest.fromJson(json.decode(response.body));
    });
  }

  Future<void> _showDialog() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Tour request sent successfully'),
          actions: <Widget>[
            FlatButton(
              child: Text(
                'Okay',
                style: TextStyle(color: Colors.white),
              ),
              color: Theme.of(context).accentColor,
              onPressed: () {
                Navigator.of(context).popUntil((route) => route.isFirst);
              },
            ),
          ],
        );
      },
    );
  }

  Future<void> _showAllReadySentDialog() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('You allready sent tour request'),
          actions: <Widget>[
            FlatButton(
              child: Text(
                'Okay',
                style: TextStyle(color: Colors.white),
              ),
              color: Theme.of(context).accentColor,
              onPressed: () {
                Navigator.of(context).popUntil((route) => route.isFirst);
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        leading: BackButton(color: Colors.white),
        title: Text('Tour House', style: TextStyle(color: Colors.white)),
      ),
      body: Container(
        padding: EdgeInsets.all(15),
        child: SingleChildScrollView(
          child: Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                TextFormField(
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    hintText: 'Enter Name',
                    hintStyle: TextStyle(fontSize: 14),
                  ),
                  keyboardType: TextInputType.text,
                  controller: nameController,
                  validator: (String value) {
                    if (value.isEmpty) {
                      return 'Please enter name';
                    } else {
                      return null;
                    }
                  },
                  onSaved: (String value) {
                    nameController.text = value;
                  },
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    hintText: 'Phone Number',
                    hintStyle: TextStyle(fontSize: 14),
                  ),
                  keyboardType: TextInputType.number,
                  controller: mobNoController,
                  maxLength: 10,
                  validator: (String value) {
                    if (value.isEmpty) {
                      return 'Please enter mobile number';
                    }
                    if (value.length < 10) {
                      return 'Please enter correct mobile number';
                    } else {
                      return null;
                    }
                  },
                  onSaved: (String value) {
                    mobNoController.text = value;
                  },
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    hintText: 'Email',
                    hintStyle: TextStyle(fontSize: 14),
                  ),
                  controller: emailController,
                  validator: (String value) {
                    Pattern pattern =
                        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                    RegExp regex = new RegExp(pattern);
                    if (value.isEmpty) {
                      return 'Please enter email';
                    } else if (!regex.hasMatch(value))
                      return 'Enter Valid Email';
                    else
                      return null;
                  },
                  onSaved: (String value) {
                    emailController.text = value;
                  },
                ),
                SizedBox(height: 20),
                Center(
                  child: Text(
                    'SCHEDULE DATE',
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                  ),
                ),
                SizedBox(
                  height: 15,
                ),
                RaisedButton(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5.0)),
                  elevation: 4.0,
                  onPressed: () {
                    DatePicker.showDatePicker(context,
                        theme: DatePickerTheme(
                          containerHeight: 100.0,
                        ),
                        showTitleActions: true,
                        minTime: DateTime(2000, 01, 1),
                        maxTime: DateTime(2022, 12, 31), onConfirm: (date) {
                      setState(() {
                        formattedDate = formatter.format(date);
                        _date = formattedDate;
                        print(formattedDate);
                      });
                    }, locale: LocaleType.en);
                  },
                  child: Container(
                    alignment: Alignment.center,
                    height: 45.0,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Container(
                              child: Row(
                                children: <Widget>[
                                  Icon(
                                    Icons.date_range,
                                    size:16,
                                    color: Colors.white,
                                  ),
                                  SizedBox(width: 5),
                                  Text(
                                    " $_date",
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 16),
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                        Text(
                          "  Change",
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: 16.0),
                        ),
                      ],
                    ),
                  ),
                  color: Theme.of(context).accentColor,
                ),
                SizedBox(
                  height: 20,
                ),
                Center(
                  child: Text(
                    'SCHEDULE TIME',
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                  ),
                ),
                SizedBox(
                  height: 15,
                ),
                Card(
                  color: Theme.of(context).accentColor,
                  child: DropdownButton(
                    elevation: 8,
                    underline: Container(color: Colors.white),
                    iconEnabledColor: Colors.white,
                    iconSize: 40,
                    isExpanded: true,
                    items: time
                        .map(
                          (value) => DropdownMenuItem(
                            child: Padding(
                                padding: EdgeInsets.only(left: 15),
                                child: Row(
                                  children: <Widget>[
                                    Icon(
                                      Icons.access_time,
                                      size: 16,
                                      color:Colors.white,
                                    ),
                                    SizedBox(width: 10),
                                    Text(
                                      value,
                                      style: TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold,
                                        color:Colors.white,
                                      ),
                                    ),
                                  ],
                                )),
                            value: value,
                          ),
                        )
                        .toList(growable: true),
                    onChanged: (newVal) {
                      setState(() {
                        _selectedTime = newVal;
                        print(_selectedTime);
                      });
                    },
                    value: _selectedTime,
                    hint: Padding(
                        padding: EdgeInsets.only(left: 10),
                        child: Text('Select Time',
                            overflow: TextOverflow.ellipsis,
                            style:
                                TextStyle(fontSize: 16, color: Colors.white))),
                  ),
                ),
                SizedBox(height: 20),
                Container(
                  height: 40,
                  child: FlatButton(
                    color: Theme.of(context).accentColor,
                    onPressed: () {
                      if (_formKey.currentState.validate()) {
                        return getTourRequest();
                      }
                    },
                    child: Text(
                      'SHEDULE TOUR',
                      style: TextStyle(fontSize: 18, color: Colors.white),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  getTourRequest() async {
    SaveTourRequest saveTourRequest = new SaveTourRequest(
        name: nameController.text,
        email: emailController.text,
        mobile: mobNoController.text,
        stayId: widget.stayId,
        homeName: widget.pgName,
        dateSlot: _date,
        timeSlot: _time,
        userId: widget.ownerId);
    await tour(saveTourRequestURL, map: saveTourRequest.toMap());
    if (statusCode == 200) {
      _showDialog();
    } else if (statusCode == 208) {
      _showAllReadySentDialog();
    }
  }
}
