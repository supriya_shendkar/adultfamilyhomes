import 'package:flutter/material.dart';
import 'bottom_navigation_bar_buttons.dart';
class OwnerDetails extends StatefulWidget {
  final String ownerImgPath,
      ownerName,
      pgName,
      mobile,
      ownerEmail,
      stayId,
      ownerId,
      slugTitle;

  OwnerDetails(
      {Key key,
        @required this.ownerImgPath,
        @required this.ownerName,
        @required this.pgName,
        @required this.mobile,
        @required this.ownerEmail,
        @required this.stayId,
        @required this.ownerId,
        @required this.slugTitle})
      : super(key: key);

  @override
  _OwnerDetailsState createState() => _OwnerDetailsState();
}

class _OwnerDetailsState extends State<OwnerDetails> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Card(
        child: Padding(
          padding: EdgeInsets.all(10),
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                widget.ownerImgPath == ""
                    ? CircleAvatar(
                  radius: 70,
                  backgroundImage: NetworkImage(
                      'https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png'),
                )
                    : CircleAvatar(
                  radius: 70,
                  backgroundImage: NetworkImage(widget.ownerImgPath),
                ),
                SizedBox(height: 50,),
                Text(widget.ownerName,
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                    textAlign: TextAlign.center),
                SizedBox(height: 50),
                Text(
                    "Owner/Provider", style: TextStyle(fontSize: 16)
                ),SizedBox(height: 50),
                RichText(
                  text: new TextSpan(
                      style: new TextStyle(
                        fontSize: 16.0,
                        color: Colors.black,
                      ),
                      children: <TextSpan>[
                        TextSpan(text: 'I am the owner of '),
                        TextSpan(
                          text: ' ${widget.pgName}.',
                          style: new TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 17,
                              color: Theme.of(context).accentColor),
                        ),
                        TextSpan(text: ' Feel free to get in touch with me.'),
                      ]),
                ),SizedBox(height: 50,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      "Mobile Number  :  ",
                      style: TextStyle(fontSize: 16),
                    ),
                    Text(widget.mobile,
                        style:
                        TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                        textAlign: TextAlign.center),
                  ],
                )
              ],
            ),
          ),
        ),
      ),
      bottomNavigationBar: Container(
        height: 50,
        color: Colors.white,
        child: BottomNavigationBarButtons(
          pgName: widget.pgName,
          ownerEmail: widget.ownerEmail,
          stayId: widget.stayId,
          ownerId: widget.ownerId,
          slugTitle: widget.slugTitle,
        ),
      ),
    );
  }
}
