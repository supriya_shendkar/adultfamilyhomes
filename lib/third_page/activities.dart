import 'package:flutter/material.dart';

class Activities extends StatefulWidget {
  List result2 = List();
  Activities({
    Key key,
    @required this.result2,
  }) : super(key: key);
  @override
  _ActivitiesState createState() => _ActivitiesState();
}

class _ActivitiesState extends State<Activities> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Activities'),
      ),
      body: Card(
        elevation: 10,
        child: ListView.separated(
          itemBuilder: (BuildContext context, int index) {
            return Padding(
              padding: EdgeInsets.all(15),
              child: Text(widget.result2[index]),
            );
          },
          separatorBuilder: (context, index) {
            return Divider();
          },
          itemCount: widget.result2.length,
        ),
      ),
    );
  }
}
