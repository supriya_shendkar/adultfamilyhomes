import 'dart:convert';
import 'package:afh/model/call_back.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import '../constant.dart';

class CallBackRequest extends StatefulWidget {
  String stayId, pgName, ownerEmail, ownerId;
  CallBackRequest({
    Key key,
    @required this.pgName,
    @required this.ownerEmail,
    @required this.stayId,
    @required this.ownerId,
  }) : super(key: key);
  @override
  _CallBackRequestState createState() => _CallBackRequestState();
}

class _CallBackRequestState extends State<CallBackRequest> {


  var _formKey = GlobalKey<FormState>();
  final callBackRequestURL = BASE_URL + "/api/myafh/getCallBack";
  TextEditingController nameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController mobNoController = TextEditingController();
  var statusCode;

  Future<CallBack> callBack(String url, {map}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return http.post(url, body: utf8.encode(json.encode(map)), headers: {
      "content-type": "application/json",
      "Accept": "application/json",
      "access-control-allow-origin": "*",
      "Authorization": "Bearer " + prefs.getString("token"),
    }).then((http.Response response) {
      statusCode = response.statusCode;
      print(statusCode);
      return CallBack.fromJson(json.decode(response.body));
    });
  }

  getCallBack() async {
    CallBack callBackRequest = new CallBack(
      callUser: nameController.text,
      callEmail: emailController.text,
      callPhone: mobNoController.text,
      stayId: widget.stayId,
      homeName: widget.pgName,
      ownerId: widget.ownerId,
      requestDate: DateTime.now().toString(),
    );
    await callBack(callBackRequestURL, map: callBackRequest.toMap());
    if (statusCode == 200) {
      _showDialog();
    }
  }

  Future<void> _showDialog() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Call back request sent successfully'),
          actions: <Widget>[
            FlatButton(
              child: Text(
                'Okay',
                style: TextStyle(color: Colors.white),
              ),
              color: Theme.of(context).accentColor,
              onPressed: () {
                Navigator.of(context).popUntil((route) => route.isFirst);
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        leading: BackButton(
          color: Colors.white,
        ),
        title: Text(
          'Call Back Request',
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(10),
          child: Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                TextFormField(
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    hintText: 'Enter Name',
                    hintStyle: TextStyle(fontSize: 14),
                  ),
                  keyboardType: TextInputType.text,
                  controller: nameController,
                  validator: (String value) {
                    if (value.isEmpty) {
                      return 'Please enter name';
                    } else {
                      return null;
                    }
                  },
                  onSaved: (String value) {
                    nameController.text = value;
                  },
                ),
                SizedBox(height: 10),
                TextFormField(
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    hintText: 'Phone Number',
                    hintStyle: TextStyle(fontSize: 14),
                  ),
                  keyboardType: TextInputType.number,
                  controller: mobNoController,
                  maxLength: 10,
                  validator: (String value) {
                    if (value.isEmpty) {
                      return 'Please enter mobile number';
                    }
                    if (value.length < 10) {
                      return 'Please enter correct mobile number';
                    } else {
                      return null;
                    }
                  },
                  onSaved: (String value) {
                    mobNoController.text = value;
                  },
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    hintText: 'Email',
                    hintStyle: TextStyle(fontSize: 14),
                  ),
                  controller: emailController,
                  validator: (String value) {
                    Pattern pattern =
                        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                    RegExp regex = new RegExp(pattern);
                    if (value.isEmpty) {
                      return 'Please enter email';
                    } else if (!regex.hasMatch(value))
                      return 'Please enter valid email';
                    else
                      return null;
                  },
                  onSaved: (String value) {
                    emailController.text = value;
                  },
                ),
                SizedBox(
                  height: 25,
                ),
                Container(
                  height: 40,
                  child: FlatButton(
                    color: Theme.of(context).accentColor,
                    onPressed: () {
                      if (_formKey.currentState.validate()) {
                        return getCallBack();
                      }
                    },
                    child: Text(
                      'GET A CALLBACK',
                      style: buttonTextStyle,
                    ),
                  ),
                ),
                SizedBox(
                  height: 15,
                ),
                Container(
                  child: Center(
                    child: Text(
                      'OR',
                      style: TextStyle(fontSize: 20),
                    ),
                  ),
                ),
                SizedBox(
                  height: 15,
                ),
                Container(
                  height: 40,
                  child: FlatButton(
                    color: Theme.of(context).accentColor,
                    onPressed: () {

                    },
                    child: Text(
                      'CALL US TODAY        ',
                      style: buttonTextStyle,
                    ),
                  ),
                ),
                SizedBox(height: 20),
                Container(
                  height: 40,
                  child: Center(
                    child: Text(
                      'TIME :  9 AM To 9 PM',
                      style: TextStyle(fontSize: 18, color: Colors.white),
                    ),
                  ),
                  color: Theme.of(context).primaryColorDark,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
