import 'package:flutter/material.dart';

class Services extends StatefulWidget {
  List result1 = List();
  Services({
    Key key,
    @required this.result1,
  }) : super(key: key);
  @override
  _ServicesState createState() => _ServicesState();
}

class _ServicesState extends State<Services> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Services'),
      ),
      body: Card(
        elevation: 10,
        child: ListView.separated(
          itemBuilder: (BuildContext context, int index) {
            return Padding(
              padding: EdgeInsets.all(15),
              child: Text(widget.result1[index]),
            );
          },
          separatorBuilder: (context, index) {
            return Divider();
          },
          itemCount: widget.result1.length,
        ),
      ),
    );
  }
}
