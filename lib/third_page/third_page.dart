import 'dart:convert';
import 'package:flutter/material.dart';
import '../constant.dart';
import 'about_this_house.dart';
import 'overview.dart';
import 'package:http/http.dart' as http;
import 'owner_details.dart';

class ThirdPage extends StatefulWidget {
  final String stayId, homeName;
  ThirdPage({Key key, @required this.stayId, @required this.homeName})
      : super(key: key);
  @override
  _ThirdPageState createState() => _ThirdPageState();
}

class _ThirdPageState extends State<ThirdPage>with SingleTickerProviderStateMixin {
  TabController tabController;
  var myData;
  var response;
  List images = List();
  List amenity = List();
  List amenityResult;
  List activity = List();
  List activityResult = List();
  List room = List();
  List roomsResult = List();
  var amenities = [
    'Asst. with Daily Living',
    'Awake Night Staff',
    '24 hour care',
    'Dementia/Alzheimer',
    'Developmental Disabilities',
    'Diabetes',
    'Home Doctor',
    'Hospice',
    'Housekeeping/Laundry',
    'Massage Therapist',
    'Medication Delivery',
    'Medication Management',
    'Oxygen Therapy',
    "Parkinson's",
    'Pet Care',
    'Podiatrist',
    'PT/OT & Sp. Therapy',
    'Respite Care - Short, - term',
    'RN Operated',
    'Stroke',
    'Visual/Hearing Impaired',
    'Wound Care',
    'Doctor On Site',
    "Feeding Tube Resident's Welcomed",
    '2 caregivers on shift',
    '3 caregiver on shift',
    'Catheter Care Provided',
    'Colostomy Bag Residents',
    'Female Residents Preferred',
    'Male Residentes Preferred',
    'Medicaid Accepted',
    'Home owner lives in the house & provides care',
    "Transportation to Doctor's Provided",
    'Transportation to Shopping Provided'
  ];
  var activities = [
    'Activities coordinator',
    'Arts and Crafts',
    'Beautician',
    'Birthday celebration',
    'Cable TV',
    'Games',
    'Internet access',
    'Library on Wheels',
    'Music',
    'Newspaper',
    'Telephone',
    'Wheelchair accessible',
    'Tv in room',
    'Emergency call buttons',
    'Generator',
    'Hospital Elelectric bed Available in the room',
    'Hoyer Lift',
    'Outdoor Common Area',
  ];
  var rooms = [
    'Private Room',
    'Room & Bath',
    'Room & Shower',
    'Semi-private Room',
    'Shared Main Shower'
  ];

  Future<String> getParticularHome() async {
    response = await http.get(
        Uri.encodeFull(BASE_URL +
            "/api/myafh/filtered_selected_data?stayId=" +
            widget.stayId),
        headers: {"Accept": "application/json"});
    if (this.mounted) {
      setState(() {
        var convertDataToJson = json.decode(response.body);
        myData = convertDataToJson['selectedfilterdata'];
        images = myData['images'];
        if (images.length == 0) {
          var data = {"imagePath": myData["imgPath"]};
          images.add(data);
        }
        amenity = myData['hexame'].split('').reversed.toList();
        amenityResult = [];
        for (var i = 0; i < amenity.length; i++) {
          if (amenity[i] == "1") {
            amenityResult.add(amenities[i]);
          }
        }
        activity = myData['hexact'].split('').reversed.toList();
        activityResult = [];
        for (var i = 0; i < activity.length; i++) {
          if (activity[i] == "1") {
            activityResult.add(activities[i]);
          }
        }
        room = myData['rooms'].split('').reversed.toList();
        roomsResult = [];
        for (var i = 0; i < room.length; i++) {
          if (room[i] == "1") {
            roomsResult.add(rooms[i]);
          }
        }
      });
    }
    return "Success";
  }

  @override
  void initState() {
    super.initState();
    tabController = TabController(length: 3, vsync: this);
    this.getParticularHome();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          widget.homeName,
          style: TextStyle(fontSize: 15),
        ),
        bottom: TabBar(
          indicatorColor: Colors.white,
          controller: tabController,
          tabs: <Widget>[
            Tab(
              child: Text(
                'OVERVIEW',
                style: TextStyle(fontSize: 12, color: Colors.white),
              ),
            ),
            Tab(
              child: Text(
                'ABOUT HOUSE',
                style: TextStyle(fontSize: 12, color: Colors.white),
              ),
            ),
            Tab(
              child: Text(
                'MEET OWNER',
                style: TextStyle(fontSize: 12, color: Colors.white),
              ),
            ),
          ],
        ),
      ),
      body: response == null
          ? Center(
              child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                CircularProgressIndicator(),
                SizedBox(
                  height: 30,
                ),
                Text('    Loading . . .'),
              ],
            ))
          : TabBarView(
              controller: tabController,
              children: <Widget>[
                OverView(
                  result1: amenityResult,
                  images: images,
                  pgName: myData['pgname'],
                  address: myData['address'],
                  medicAid: myData['medicAid'],
                  imagePath: myData['imagePath'],
                  result2: activityResult,
                  cityName: myData['pgcity'],
                  ownerName: myData['ownername'],
                  webURL: myData['weburl'],
                  result3: roomsResult,
                  zip: myData['zip'],
                  airportDist: myData['airpoDist'],
                  busStatDist: myData['busStatDist'],
                  coffeeShopDist: myData['coffShopDist'],
                  hospitalDist: myData['hospiDist'],
                  ownerEmail: myData['ownerEmail'],
                  shopCentDist: myData['shopCentDist'],
                  stayId: myData['stayId'],
                  ownerId: myData['userId'],
                  slugTitle: myData['slugTitle'],
                ),
                AboutThisHouse(
                  description: myData['description'],
                  imagePath: myData['imgPath'],
                  ownerId: myData['userId'],
                  stayId: myData['stayId'],
                  slugTitle: myData['slugTitle'], ownerEmail: myData['ownerEmail'], pgName: myData['pgname'],),
                OwnerDetails(
                  ownerName: myData['ownername'],
                  ownerImgPath: myData['ownerImgPath'],
                  mobile: myData['mobile'],
                  pgName: myData['pgname'],
                  ownerId: myData['userId'],stayId: myData['stayId'],
                  ownerEmail: myData['ownerEmail'], slugTitle: myData['slugTitle']),
              ],
            ),
    );
  }
}
