import 'package:flutter/material.dart';

const String BASE_URL = 'http://afhtest.mircloud.us';

final headerStyle = TextStyle(
  fontSize: 18,
  fontWeight: FontWeight.bold,
);
final subHeaderStyle = TextStyle(
  fontSize: 14,
  fontWeight: FontWeight.bold,
);
final textFormFieldStyle = TextStyle(
  fontSize: 14,
);
final bottomButtonTextStyle =
    TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 18);
final bottomButtonStyle = TextStyle(
  color: Colors.white,
  fontSize: 13,
);
final buttonTextStyle = TextStyle(
  color: Colors.white,
  fontSize: 15,
);
