import 'dart:convert';
import 'package:afh/third_page/third_page.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:page_transition/page_transition.dart';
import '../constant.dart';


class PremiumHomes extends StatefulWidget {
  @override
  _PremiumHomesState createState() => _PremiumHomesState();
}

class _PremiumHomesState extends State<PremiumHomes> {
  @override
  void initState() {
    super.initState();
    this.getPremiumHomes();
  }

  List premiumHomeData = List();

  Future<String> getPremiumHomes() async {
    var response = await http.get(
        Uri.encodeFull(BASE_URL + "/afh/homes/premium"),
        headers: {"Accept": "application/json"});
    if (this.mounted) {
      setState(() {
        premiumHomeData = json.decode(response.body);
      });
    }

    return "Success";
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      padding: EdgeInsets.fromLTRB(5, 10, 0, 5),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(left: 5),
            child: Text(
              'Premium Homes',
              style: TextStyle(
                fontSize: 22,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          SizedBox(height: 10),
          Container(
            child: ListView.builder(
              scrollDirection: Axis.horizontal,
              itemBuilder: (BuildContext context, int index) {
                return GestureDetector(
                  onTap: () {
                    Navigator.push(
                      context,
                      PageTransition(
                          type: PageTransitionType.rightToLeft,
                          child: ThirdPage(
                            stayId: premiumHomeData[index]['stayId'],
                            homeName: premiumHomeData[index]['homeName'],
                          )),
                    );
                  },
                  child: Card(
                    elevation: 5,
                    color: Colors.white,
                    child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        ClipRRect(
                          borderRadius: BorderRadius.all(Radius.circular(4)),
                          child: new Image.network(
                            "" + premiumHomeData[index]['imgPath'],
                            fit: BoxFit.fill,
                            height: 150,
                            width: 200,
                          ),
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Container(
                          padding: EdgeInsets.only(left: 5),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              SizedBox(
                                height: 5,
                              ),
                              Text(
                                "" + premiumHomeData[index]['homeName'],
                                style: TextStyle(
                                  fontSize: 11,
                                ),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Text(
                                "" + premiumHomeData[index]['city'],
                                style: TextStyle(
                                  fontSize: 11,
                                ),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Row(
                                children: <Widget>[
                                  premiumHomeData[index]['medicAid'] == 'yes'
                                      ? new Image.asset('Icons/medicAid.png',
                                          scale: 1.3)
                                      : Container(),
                                  SizedBox(width: 5),
                                  premiumHomeData[index]['medicAid'] == 'yes'
                                      ? new Text(
                                          'Provides MedicAid',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .primaryColorDark),
                                        )
                                      : Container(),
                                ],
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                );
              },
              itemCount: premiumHomeData == null ? 0 : premiumHomeData.length,
            ),
            height: 250,
          ),
        ],
      ),
    );
  }
}
