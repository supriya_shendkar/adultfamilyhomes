import 'package:flutter/material.dart';
class SingleMessage extends StatefulWidget {
  final String homeName, ownerName, ownerImg;
  SingleMessage(
      {Key key,
      @required this.homeName,
      @required this.ownerName,
      @required this.ownerImg})
      : super(key: key);
  @override
  _SingleMessageState createState() => _SingleMessageState();
}

class _SingleMessageState extends State<SingleMessage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Row(
          children: <Widget>[
            CircleAvatar(
              backgroundImage: NetworkImage(widget.ownerImg),
              maxRadius: 20,
            ),
            SizedBox(
              width: 10,
            ),
            Text(widget.ownerName, style: TextStyle(fontSize: 18)),
          ],
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.delete),
            onPressed: () {},
          ),
        ],
      ),
      body: Padding(padding: EdgeInsets.all(15),
        child: Flex(children: <Widget>[TextFormField(
          decoration: InputDecoration(
            border: OutlineInputBorder(),
            hintText: 'Please wirte something here',
            hintStyle: TextStyle(fontSize: 14),
          ),
          keyboardType: TextInputType.text,
          validator: (String value) {
            if (value.isEmpty) {
              return 'Please enter something here';
            } else {
              return null;
            }
          },
          onSaved: (String value) {
          },
        ),],)
      ),
    );
  }
}
