
import 'package:afh/AdvancedSearch/advanced_filter_search.dart';
import 'package:afh/profile/more.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'home_screen.dart';
import 'messages.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  void initState() {
    super.initState();
    currentPage = HomeScreen();
  }

  int currentTab = 0;
  Widget currentPage;
  List<Widget> pages = [
    HomeScreen(),
    AdvancedFilterSearch(),
    MessageScreen(),
    More()
  ];

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final BottomNavigationBar navBar = new BottomNavigationBar(
      selectedItemColor: Theme.of(context).primaryColorDark,
      unselectedItemColor: Colors.grey,
      type: BottomNavigationBarType.fixed,
      currentIndex: currentTab,
      onTap: (int numTab) {
        setState(() {
          currentTab = numTab;
          currentPage = pages[numTab];
        });
      },
      items: <BottomNavigationBarItem>[
        BottomNavigationBarItem(
          icon: new Icon(
            Icons.home,
          ),
          title: new Text(
            'Home',
            style: TextStyle(fontSize: 13),
          ),
        ),
        BottomNavigationBarItem(
          icon: new Icon(
            Icons.search,
          ),
          title: new Text(
            'Search',
            style: TextStyle(fontSize: 13),
          ),
        ),
        BottomNavigationBarItem(
          icon: Icon(
            Icons.message,
          ),
          title: new Text(
            'Message',
            style: TextStyle(fontSize: 13),
          ),
        ),
        /* BottomNavigationBarItem(
          icon: Icon(
            Icons.home,
          ),
          title: new Text(
            'Marketplace',
            style: TextStyle(fontSize: 13),
          ),
        ),*/
        BottomNavigationBarItem(
          icon: Icon(
            Icons.person,
          ),
          title: new Text(
            'More',
            style: TextStyle(fontSize: 13),
          ),
        ),
      ],
    );

    return Scaffold(
      bottomNavigationBar:
          navBar, // Assigning our navBar to the Scaffold's bottomNavigationBar property.
      body: currentPage,
    );
  }
}
