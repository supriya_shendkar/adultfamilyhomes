import 'dart:convert';
import 'package:afh/third_page/third_page.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import '../constant.dart';
import 'premium_homes.dart';
import '../second_page/second_page_screen.dart';
import 'package:http/http.dart' as http;
import 'cities.dart';


class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  void initState() {
    super.initState();
    this.getPopularHomes();
  }

  List popularHomesData = List();
  var _formKey = GlobalKey<FormState>();
  TextEditingController cityNameController = TextEditingController();

  Future<String> getPopularHomes() async {
    var response = await http.get(
        Uri.encodeFull(BASE_URL + "/afh/homes/popular"),
        headers: {"Accept": "application/json"});
    if (this.mounted) {
      setState(() {
        popularHomesData = json.decode(response.body);
      });
    }return "Success";
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Image.asset(
          'Icons/Adultfamilyhomelogowhite.png',
          fit: BoxFit.contain,
          height: 50,
        ),
      ),
      resizeToAvoidBottomPadding: false,
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Form(
              key: _formKey,
              child: Padding(
                padding: EdgeInsets.all(10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    TextFormField(
                      autofocus: false,
                      autocorrect: true,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        hintText: 'Enter city name',
                        hintStyle: TextStyle(fontSize: 14),
                      ),
                      keyboardType: TextInputType.text,
                      controller: cityNameController,
                      validator: (String value) {
                        if (value.isEmpty) {
                          return 'Please enter city name';
                        } else {
                          return null;
                        }
                      },
                      onSaved: (String value) {
                        cityNameController.text;
                      },
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Container(
                      height: 40,
                      child: FlatButton(
                        color: Theme.of(context).accentColor,
                        onPressed: () {
                          if (_formKey.currentState.validate()) {
                            return Navigator.push(
                              context,
                              PageTransition(
                                type: PageTransitionType.rightToLeft,
                                child: SecondPageScreen(
                                  cityName: cityNameController.text,
                                ),
                              ),
                            );
                          }
                          return "Success";
                        },
                        child: Text(
                          'SEARCH',
                          style: TextStyle(fontSize: 18, color: Colors.white),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Container(
              color: Colors.white,
              padding: EdgeInsets.fromLTRB(0, 20, 0, 20),
              child: Cities(),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              color: Colors.white,
              child: PremiumHomes(),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              color: Colors.white,
              padding: EdgeInsets.fromLTRB(5, 10, 0, 5),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(left: 5),
                    child: Text(
                      'Popular Homes',
                      style: TextStyle(
                        fontSize: 22,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),

                  Container(
                    child: ListView.builder(
                        scrollDirection: Axis.horizontal,
                        itemBuilder: (BuildContext context, int index) {
                          return GestureDetector(
                            onTap: () {
                              Navigator.push(
                                context,
                                PageTransition(
                                    type: PageTransitionType.rightToLeft,
                                    child: ThirdPage(
                                      stayId: popularHomesData[index]['stayId'],
                                      homeName: popularHomesData[index]
                                          ['homeName'],
                                    )),
                              );
                            },
                            child: Card(
                              elevation: 5,
                              color: Colors.white,
                              child: new Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  ClipRRect(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(4)),
                                    child: new Image.network(
                                      "" + popularHomesData[index]['imgPath'],
                                      fit: BoxFit.fill,
                                      height: 150,
                                      width: 200,
                                    ),
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Container(
                                    padding: EdgeInsets.only(left: 5),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        SizedBox(
                                          height: 5,
                                        ),
                                        Text(
                                          "" +
                                              popularHomesData[index]
                                                  ['homeName'],
                                          style: TextStyle(
                                            fontSize: 11,
                                          ),
                                        ),
                                        SizedBox(
                                          height: 10,
                                        ),
                                        Text(
                                          "" + popularHomesData[index]['city'],
                                          style: TextStyle(
                                            fontSize: 11,
                                          ),
                                        ),
                                        SizedBox(
                                          height: 10,
                                        ),
                                        Row(
                                          children: <Widget>[
                                            popularHomesData[index]
                                                        ['medicAid'] ==
                                                    'yes'
                                                ? new Image.asset(
                                                    'Icons/medicAid.png',
                                                    scale: 1.3)
                                                : Container(),
                                            SizedBox(width: 5),
                                            popularHomesData[index]
                                                        ['medicAid'] ==
                                                    'yes'
                                                ? new Text(
                                                    'Provides MedicAid',
                                                    style: TextStyle(
                                                        color: Theme.of(context)
                                                            .primaryColorDark),
                                                  )
                                                : Container(),
                                          ],
                                        )
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          );
                        },
                        itemCount: popularHomesData == null
                            ? 0
                            : popularHomesData.length),
                    height: 250,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
