import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:page_transition/page_transition.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../constant.dart';
import 'single_message.dart';

class MessageScreen extends StatefulWidget {
  @override
  _MessageScreenState createState() => _MessageScreenState();
}

class _MessageScreenState extends State<MessageScreen> {
  var messages;
  var response;
  Future<String> getMessages() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    response = await http.get(
        Uri.encodeFull(BASE_URL + "/api/myafh/owner/getmsgs?role=user"),
        headers: {
          "Accept": "application/json",
          "Authorization": "Bearer " + prefs.getString("token"),
        });
    if (this.mounted) {
      setState(() {
        messages = json.decode(response.body);
      });
    }
    return "Success";
  }

  @override
  void initState() {
    super.initState();
    this.getMessages();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Inbox',
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: response == null
          ? Center(
              child: CircularProgressIndicator(),
            )
          : ListView.builder(
              itemBuilder: (BuildContext context, int index) {
                return GestureDetector(
                  onTap: () {
                    Navigator.push(
                      context,
                      PageTransition(
                        type: PageTransitionType.rightToLeft,
                        child: SingleMessage(
                          ownerName: messages[index]['oname'],
                          ownerImg: messages[index]['ownerImg'],
                          homeName: messages[index]['homename'],
                        ),
                      ),
                    );
                  },
                  child: ListTile(
                    leading: CircleAvatar(
                      backgroundImage:
                          NetworkImage("" + messages[index]['ownerImg']),
                      maxRadius: 25,
                    ),
                    title: Text(
                      "" + messages[index]['oname'],
                      style: TextStyle(fontSize: 18),
                    ),
                    subtitle: Text("" + messages[index]['homename']),
                  ),
                );
              },
              itemCount: messages == null ? 0 : messages.length,
            ),
    );
  }
}
