import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import '../second_page/second_page_screen.dart';

// ignore: must_be_immutable
class Cities extends StatefulWidget {
  String city;
  Cities({this.city});
  @override
  _CitiesState createState() => _CitiesState();
}

class _CitiesState extends State<Cities> {
  var response;
  var cityList;
  List cityImages = List();

  Future<String> getCityList() async {
    response = await http.get(
        Uri.encodeFull("http://afhtest.mircloud.us/api/myafh/cityimages"),
        headers: {"Accept": "application/json"});
    if (this.mounted) {
      setState(() {
        cityList = json.decode(response.body);
        cityImages = cityList['info'];
      });
    }
    return "Success";
  }

  @override
  void initState() {
    super.initState();
    this.getCityList();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 100,
      color: Colors.white,
      child: Center(
        child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemBuilder: (BuildContext context, int index) {
            return GestureDetector(
              onTap: () {
                Navigator.push(
                  context,
                  PageTransition(
                    type: PageTransitionType.rightToLeft,
                    child: SecondPageScreen(
                      cityName: cityImages[index]['cityName'],
                    ),
                  ),
                );
              },
              child: Container(
                margin: EdgeInsets.only(top: 0, left: 5, bottom: 0, right: 10),
                child: Column(
                  children: <Widget>[
                    CircleAvatar(
                      backgroundColor: Colors.white,
                      radius: 35,
                      backgroundImage:
                          NetworkImage("" + cityImages[index]['image']),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Text(
                      "" + cityImages[index]['cityName'],
                      style: TextStyle(fontSize: 12),
                    ),
                  ],
                ),
              ),
            );
          },
          itemCount: cityImages == null ? 0 : cityImages.length,
        ),
      ),
    );
  }
}
