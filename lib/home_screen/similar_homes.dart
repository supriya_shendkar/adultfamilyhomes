
import 'package:afh/third_page/third_page.dart';
import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:page_transition/page_transition.dart';

import '../constant.dart';
import '../second_page/second_page_screen.dart';


class SimilarHomes extends StatefulWidget {
  final String cityName;
  SimilarHomes({Key key, @required this.cityName}) : super(key: key);
  @override
  _SimilarHomesState createState() => _SimilarHomesState();
}

class _SimilarHomesState extends State<SimilarHomes> {
  List similarHomesData = List();
  Future<String> getSimilarHomes() async {
    var response = await http.get(
        Uri.encodeFull(
            BASE_URL + "/api/myafh/getAllSimilarHomes?city=" + widget.cityName),
        headers: {"Accept": "application/json"});
    if (this.mounted) {
      setState(() {
        var similarHomes = json.decode(response.body);
        similarHomesData = similarHomes['similarhomes'];
      });
    }
    return "Success";
  }

  @override
  void initState() {
    super.initState();
    this.getSimilarHomes();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      padding: EdgeInsets.only(left: 10, top: 0, right: 0, bottom: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
              padding: EdgeInsets.only(left: 5, right: 10),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Text(
                      'Similar Homes',
                      style: TextStyle(
                        fontSize: 22,
                        // fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  FlatButton(
                    child: Text(
                      'VIEW MORE',
                      style: TextStyle(
                          color: Theme.of(context).accentColor, fontSize: 13),
                    ),
                    onPressed: () {
                      Navigator.push(
                        context,
                        PageTransition(
                            type: PageTransitionType.rightToLeft,
                            child: SecondPageScreen(
                              cityName: widget.cityName,
                            )),
                      );
                    },
                  )
                ],
              )),
          SizedBox(
            height: 10,
          ),
          Container(
            child: ListView.builder(
              scrollDirection: Axis.horizontal,
              itemBuilder: (BuildContext context, int index) {
                return GestureDetector(
                  onTap: () {
                    Navigator.push(
                      context,
                      PageTransition(
                          type: PageTransitionType.rightToLeft,
                          child: ThirdPage(
                            stayId: similarHomesData[index]['stayId'],
                            homeName: similarHomesData[index]['userHouseName'],
                          )),
                    );
                  },
                  child: Card(
                    elevation: 5,
                    color: Colors.white,
                    child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        ClipRRect(
                          borderRadius: BorderRadius.all(Radius.circular(4)),
                          child: new Image.network(
                            "" + similarHomesData[index]['imagePath'],
                            fit: BoxFit.fill,
                            height: 150,
                            width: 200,
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Container(
                          padding: EdgeInsets.only(left: 5),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                "" + similarHomesData[index]['userHouseName'],
                                style: TextStyle(
                                  fontSize: 12,
                                ),
                              ),
                              SizedBox(
                                width: 20,
                              ),
                              Text(
                                "" + similarHomesData[index]['userCity'],
                                style: TextStyle(
                                  fontSize: 11,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                );
              },
              itemCount: similarHomesData == null ? 0 : similarHomesData.length,
            ),
            height: 250,
          ),
        ],
      ),
    );
  }
}
