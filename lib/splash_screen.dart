import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'home_screen/Home.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    Timer(
      Duration(seconds: 2),
      () {
        Navigator.of(context).pushReplacement(
          MaterialPageRoute(
            builder: (BuildContext context) => Home(),
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      body: Card(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          ClipRect(
            child: Image.asset(
              'Icons/adultfamilyhomeslogo.png',
              height: 150,
            ),
          ),
          SizedBox(
            height: 120,
          ),

          Text(
            ' Get connected directly with\n     senior care providers. . . . . .',
            style: TextStyle(
                fontFamily: 'OpenSans-LightItalic',
                color: Theme.of(context).primaryColorDark,
                fontSize: 25,
                fontWeight: FontWeight.bold),
          ),
        ],
      )),
    );
  }
}
