import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import '../constant.dart';

class FAQTile extends StatefulWidget {
  @override
  _FAQTileState createState() => _FAQTileState();
}

class _FAQTileState extends State<FAQTile> {
  List<FAQ> data = [];
  var response;
  List<Object> convertDataToJson;
  Future<String> faq() async {
    response = await http.get(Uri.encodeFull(BASE_URL + "/afh/faqs"),
        headers: {"Accept": "application/json"});
    if (this.mounted) {
      setState(() {
        convertDataToJson = json.decode(response.body);
      });
      for (Map i in convertDataToJson) {
        data.add(
          FAQ(
            i['question'],
            <FAQ>[
              FAQ(i['answer']),
            ],
          ),
        );
      }
    }
    return "Success";
  }

  @override
  void initState() {
    super.initState();
    this.faq();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: BackButton(
          color: Colors.white,
        ),
        title: Text(
          'FAQ\'s',
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: response == null
          ? Center(
              child: CircularProgressIndicator(),
            )
          : new ListView.builder(
              itemBuilder: (BuildContext context, int index) =>
                  EntryItem(data[index]),
              itemCount: data.length,
            ),
    );
  }
}

class FAQ {
  final String title;
  final List<FAQ> children;
  FAQ(this.title, [this.children = const <FAQ>[]]);
}

class EntryItem extends StatelessWidget {
  const EntryItem(this.entry);
  final FAQ entry;

  Widget _buildTiles(FAQ root) {
    if (root.children.isEmpty)
      return ListTile(
        title: Text(
          root.title,
          style: TextStyle(color: Colors.grey, fontSize: 14),
        ),
      );
    return new ExpansionTile(
      key: PageStorageKey<FAQ>(root),
      title: Text(
        root.title,
        style: TextStyle(fontSize: 16, color: Colors.black),
      ),
      children: root.children.map(_buildTiles).toList(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Card(elevation: 5, child: _buildTiles(entry));
  }
}
