import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

import '../constant.dart';

class DepositHistory extends StatefulWidget {
  @override
  _DepositHistoryState createState() => _DepositHistoryState();
}

class _DepositHistoryState extends State<DepositHistory> {
  List depositHistory = List();
  var depositHistoryResponse;

  Future<String> getDepositHistory() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    depositHistoryResponse = await http.get(
        Uri.encodeFull(BASE_URL + "/api/myafh/owner/getTokenDetails"),
        headers: {
          "Accept": "application/json",
          "access-control-allow-origin": "*",
          "Authorization": "Bearer " + prefs.getString("token"),
        });
    setState(() {
      var jsonData = json.decode(depositHistoryResponse.body);
      depositHistory = jsonData['listPayPojo'];

      print(depositHistory);
    });
    return "Success";
  }

  @override
  void initState() {
    super.initState();
    this.getDepositHistory();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: BackButton(
          color: Colors.white,
        ),
        title: Text(
          'Deposit History',
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: depositHistoryResponse == null
          ? Center(
              child: Text('Loading...'),
            )
          : depositHistory.isEmpty
              ? Card(
                  child: Center(
                    child: Text('No Deposit History found'),
                  ),
                )
              : ListView.builder(
                  itemBuilder: (BuildContext context, int index) {
                    return Card(
                      child: Column(
                        children: <Widget>[
                          ClipRRect(
                            borderRadius: BorderRadius.all(Radius.circular(4)),
                            child: Image.network(
                              depositHistory[index]['houseImage'],
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Container(
                            padding: EdgeInsets.all(5),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: <Widget>[
                                Text(
                                  depositHistory[index]['houseName'],
                                  style: headerStyle,
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Row(
                                  children: <Widget>[
                                    Text(
                                      'Date  :  ',
                                      style: subHeaderStyle,
                                    ),
                                    Text(
                                      depositHistory[index]['txDate'],
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Row(
                                  children: <Widget>[
                                    Text(
                                      'Deposit Amount paid  :  ',
                                      style: subHeaderStyle,
                                    ),
                                    Text(
                                      depositHistory[index]['amount'],
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Container(
                                  height: 40,
                                  child: FlatButton(
                                    color: Theme.of(context).accentColor,
                                    onPressed: () {},
                                    child: Text(
                                      'Download Invoice',
                                      style: buttonTextStyle,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    );
                  },
                  itemCount: depositHistory == null ? 0 : depositHistory.length,
                ),
    );
  }
}
