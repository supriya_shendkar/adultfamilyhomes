import 'dart:convert';
import 'package:afh/third_page/call_back_request.dart';
import 'package:afh/third_page/checkout.dart';
import 'package:afh/third_page/third_page.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

import '../constant.dart';


class UserTourRequest extends StatefulWidget {
  @override
  _UserTourRequestState createState() => _UserTourRequestState();
}

class _UserTourRequestState extends State<UserTourRequest> {
  List tourRequest = List();
  var tourRequestResponse;
  Future<String> getOwnerBooking() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    tourRequestResponse = await http.get(
        Uri.encodeFull(BASE_URL + "/api/myafh/owner/getOwnerBookings"),
        headers: {
          "Accept": "application/json",
          "access-control-allow-origin": "*",
          "Authorization": "Bearer " + prefs.getString("token"),
        });
    setState(() {
      var jsonData = json.decode(tourRequestResponse.body);
      tourRequest = jsonData['listBook'];
      print(tourRequest);
    });
    return "Success";
  }

  @override
  void initState() {
    super.initState();
    this.getOwnerBooking();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: BackButton(
          color: Colors.white,
        ),
        title: Text(
          'Tour Request',
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: tourRequestResponse == null
          ? Center(
              child: Text('Loading...'),
            )
          : tourRequest.isEmpty
              ? Card(
                  child: Center(
                    child: Text('No Tour Request found'),
                  ),
                )
              : ListView.builder(
                  padding: EdgeInsets.all(5),
                  itemBuilder: (BuildContext context, int index) {
                    return Card(
                      elevation: 10,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          ClipRRect(
                            borderRadius: BorderRadius.all(Radius.circular(4)),
                            child: Image.network(
                              tourRequest[index]['homeImage'],
                              fit: BoxFit.fill,
                              height: 200,
                              width: 350,
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.only(left: 5, top: 5),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  tourRequest[index]['homeName'],
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16),
                                ),
                                SizedBox(
                                  height: 3,
                                ),
                                Text(
                                  tourRequest[index]['address'],
                                  style: TextStyle(color: Colors.grey),
                                ),
                                SizedBox(
                                  height: 3,
                                ),
                                Row(
                                  children: <Widget>[
                                    Text(
                                      'Date             :  ',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold),
                                    ),
                                    tourRequest[index]['dateSlot'] == null
                                        ? Container()
                                        : Text(
                                            tourRequest[index]['dateSlot'],
                                          ),
                                  ],
                                ),
                                SizedBox(
                                  height: 3,
                                ),
                                Row(
                                  children: <Widget>[
                                    Text(
                                      'Time            :  ',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold),
                                    ),
                                    tourRequest[index]['timeSlot'] == null
                                        ? Container()
                                        : Text(
                                            tourRequest[index]['timeSlot'],
                                          ),
                                  ],
                                ),
                                SizedBox(
                                  height: 3,
                                ),
                                Row(
                                  children: <Widget>[
                                    Text(
                                      'Tour status :  ',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold),
                                    ),
                                    tourRequest[index]['timeSlot'] == 'yes'
                                        ? Text('Accepted')
                                        : Text('Pending'),
                                  ],
                                ),
                              ],
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.all(5),
                            child: Row(
                              children: <Widget>[
                                Expanded(
                                  child: Container(
                                    height: 40,
                                    child: FlatButton(
                                      onPressed: () {
                                        Navigator.push(
                                          context,
                                          PageTransition(
                                            type:
                                                PageTransitionType.rightToLeft,
                                            child: ThirdPage(
                                              stayId: tourRequest[index]
                                                  ['stayId'],
                                              homeName: tourRequest[index]
                                                  ['homeName'],
                                            ),
                                          ),
                                        );
                                      },
                                      color: Theme.of(context).accentColor,
                                      child: Text(
                                        'View Listing',
                                        style: TextStyle(color: Colors.white),
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  width: 2,
                                ),
                                Expanded(
                                  child: Container(
                                    height: 40,
                                    child: FlatButton(
                                      onPressed: () {
                                        Navigator.push(
                                          context,
                                          PageTransition(
                                            type:
                                                PageTransitionType.rightToLeft,
                                            child: CheckOut(),
                                          ),
                                        );
                                      },
                                      color: Theme.of(context).accentColor,
                                      child: Text(
                                        'Reserve Bed',
                                        style: TextStyle(color: Colors.white),
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  width: 2,
                                ),
                                Expanded(
                                  child: Container(
                                    height: 40,
                                    child: FlatButton(
                                      onPressed: () {
                                        Navigator.push(
                                          context,
                                          PageTransition(
                                            type:
                                                PageTransitionType.rightToLeft,
                                            child: CallBackRequest(),
                                          ),
                                        );
                                      },
                                      color: Theme.of(context).accentColor,
                                      child: Text(
                                        'Callback',
                                        style: TextStyle(color: Colors.white),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    );
                  },
                  itemCount: tourRequest == null ? 0 : tourRequest.length,
                ),
    );
  }
}
