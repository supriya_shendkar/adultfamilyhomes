import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';

import 'faq.dart';
class HowItWorks extends StatefulWidget {
  @override
  _HowItWorksState createState() => _HowItWorksState();
}

class _HowItWorksState extends State<HowItWorks> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'How it works',
          style: TextStyle(color: Colors.white),
        ),
        leading: BackButton(
          color: Colors.white,
        ),
      ),
      body: SafeArea(
        child: Container(
          padding: EdgeInsets.all(5),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Card(
                  elevation: 10,
                  child: Container(
                    padding: EdgeInsets.fromLTRB(10, 5, 5, 0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          'Step 1',
                          style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        Row(
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.fromLTRB(30, 10, 0, 10),
                              child: Image.asset(
                                'Icons/Search.png',
                                color: Theme.of(context).primaryColorDark,
                                scale: 3.0,
                              ),
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Container(
                              padding: EdgeInsets.fromLTRB(10, 0, 0, 10),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    'Search',
                                    style: TextStyle(
                                        decoration: TextDecoration.underline,
                                        fontSize: 22),
                                  ),
                                  Text(
                                    'Search adult family homes by \ncity and zip code',
                                    style: TextStyle(fontSize: 15),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
                Card(
                  elevation: 10,
                  child: Container(
                    padding: EdgeInsets.fromLTRB(10, 5, 5, 0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          'Step 2',
                          style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Row(
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.fromLTRB(30, 10, 0, 10),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    'Filter',
                                    style: TextStyle(
                                        decoration: TextDecoration.underline,
                                        fontSize: 22),
                                  ),
                                  Text(
                                    'Compare homes and select \nservices according to your \nneeds.',
                                    style: TextStyle(fontSize: 15),
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(
                              width: 20,
                            ),
                            Container(
                              padding: EdgeInsets.fromLTRB(10, 10, 0, 10),
                              child: Image.asset(
                                'Icons/Filter.png',
                                color: Theme.of(context).primaryColorDark,
                                scale: 3.0,
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
                Card(
                  elevation: 10,
                  child: Container(
                    padding: EdgeInsets.fromLTRB(10, 5, 5, 0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          'Step 3',
                          style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        Row(
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.fromLTRB(30, 10, 0, 10),
                              child: Image.asset(
                                'Icons/Connect.png',
                                color: Theme.of(context).primaryColorDark,
                                scale: 3.0,
                              ),
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Container(
                              padding: EdgeInsets.fromLTRB(10, 0, 0, 10),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    'Connect',
                                    style: TextStyle(
                                        decoration: TextDecoration.underline,
                                        fontSize: 22),
                                  ),
                                  Text(
                                    'Get in touch directly with \nthe adult family home with \nany questions.',
                                    style: TextStyle(fontSize: 15),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
                Card(
                  elevation: 10,
                  child: Container(
                    padding: EdgeInsets.fromLTRB(10, 5, 5, 0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          'Step 4',
                          style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        Row(
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.fromLTRB(30, 10, 0, 10),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    'Move In',
                                    style: TextStyle(
                                        decoration: TextDecoration.underline,
                                        fontSize: 22),
                                  ),
                                  Text(
                                    'Follow online steps to set up a \ntour of the home and to hold a \nor book a room',
                                    style: TextStyle(fontSize: 15),
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(
                              width: 15,
                            ),
                            Container(
                              padding: EdgeInsets.fromLTRB(10, 10, 0, 10),
                              child: Image.asset(
                                'Icons/Movein.png',
                                color: Theme.of(context).primaryColorDark,
                                scale: 3.0,
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
                Container(
                  height: 60,
                  padding: EdgeInsets.all(10),
                  child: RaisedButton(
                    color: Theme.of(context).accentColor,
                    onPressed: () {
                      setState(() {
                        Navigator.push(
                          context,
                          PageTransition(
                            type: PageTransitionType.rightToLeft,
                            child: FAQTile(),
                          ),
                        );
                      });
                    },
                    child: Row(
                      children: <Widget>[
                        Text(
                          'FAQ\'s',
                          style: TextStyle(fontSize: 15, color: Colors.white),
                        ),
                        SizedBox(
                          width: 230,
                        ),
                        Icon(
                          Icons.arrow_forward_ios,
                          color: Colors.white,
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
