
import 'package:afh/model/feedback_form.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import '../constant.dart';

class FeedBackPage extends StatefulWidget {
  @override
  _FeedBackPageState createState() => _FeedBackPageState();
}

class _FeedBackPageState extends State<FeedBackPage> {
  var statusCode;
  bool terms = false;
  final feedBackURL = BASE_URL + "/api/myafh/contactform";
  var _formKey = GlobalKey<FormState>();
  TextEditingController nameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController mobNoController = TextEditingController();
  TextEditingController messageController = TextEditingController();
  TextEditingController subjectController = TextEditingController();

  Future<FeedBackForm> feedBack(String url, {map}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return http.post(url, body: utf8.encode(json.encode(map)), headers: {
      "content-type": "application/json",
      "Accept": "application/json",
      "access-control-allow-origin": "*",
      "Authorization": "Bearer " + prefs.getString("token"),
    }).then((http.Response response) {
      statusCode = response.statusCode;
      print(statusCode);
      return FeedBackForm.fromJson(json.decode(response.body));
    });
  }

  Future<void> _showDialog() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(
              'We have received your message and would like to thank you for writing to us.\nTalk to you soon,'),
          actions: <Widget>[
            FlatButton(
              child: Text(
                'Okay',
                style: TextStyle(color: Colors.white),
              ),
              color: Theme.of(context).accentColor,
              onPressed: () {
                Navigator.of(context).popUntil((route) => route.isFirst);
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: BackButton(
          color: Colors.white,
        ),
        title: Text(
          'Contact Us',
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(10),
          child: Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                TextFormField(
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    hintText: 'Name',
                    hintStyle: TextStyle(fontSize: 14),
                  ),
                  keyboardType: TextInputType.text,
                  controller: nameController,
                  validator: (String value) {
                    if (value.isEmpty) {
                      return 'Please enter name';
                    } else {
                      return null;
                    }
                  },
                  onSaved: (String value) {
                    nameController.text = value;
                  },
                ),
                SizedBox(height: 15),
                TextFormField(
                  decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      hintText: 'Phone Number',
                      hintStyle: TextStyle(fontSize: 14)),
                  controller: mobNoController,
                  maxLength: 10,
                  keyboardType: TextInputType.number,
                  validator: (String value) {
                    if (value.isEmpty) {
                      return 'Please enter mobile number';
                    }
                    if (value.length < 10) {
                      return 'Please enter correct mobile number';
                    } else {
                      return null;
                    }
                  },
                  onSaved: (String value) {
                    mobNoController.text = value;
                  },
                ),
                SizedBox(height: 15),
                TextFormField(
                  decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      hintText: 'Email',
                      hintStyle: TextStyle(fontSize: 14)),
                  controller: emailController,
                  validator: (String value) {
                    Pattern pattern =
                        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                    RegExp regex = new RegExp(pattern);
                    if (value.isEmpty) {
                      return 'Please enter email';
                    } else if (!regex.hasMatch(value))
                      return 'Enter Valid Email';
                    else
                      return null;
                  },
                  onSaved: (String value) {
                    emailController.text = value;
                  },
                ),
                SizedBox(height: 15),
                TextFormField(
                  decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      hintText: 'Subject',
                      hintStyle: TextStyle(fontSize: 14)),
                  keyboardType: TextInputType.text,
                  controller: subjectController,
                  validator: (String value) {
                    if (value.isEmpty) {
                      return 'Please enter subject';
                    } else {
                      return null;
                    }
                  },
                  onSaved: (String value) {
                    subjectController.text = value;
                  },
                ),
                SizedBox(height: 15),
                TextFormField(
                  decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      hintText: 'Write us about any querry or issue',
                      hintStyle: TextStyle(fontSize: 14)),
                  maxLines: 5,
                  keyboardType: TextInputType.text,
                  controller: messageController,
                  validator: (String value) {
                    if (value.isEmpty) {
                      return 'Please enter something here';
                    } else {
                      return null;
                    }
                  },
                  onSaved: (String value) {
                    messageController.text = value;
                  },
                ),
                SizedBox(
                  height: 10,
                ),
                CheckboxListTile(
                  value: terms,
                  onChanged: (val) {
                    if (terms == false) {
                      setState(() {
                        terms = true;
                      });
                    } else if (terms == true) {
                      setState(() {
                        terms = false;
                      });
                    }
                  },
                  title: new Text(
                    'I have agreed Terms and conditions.',
                    style: TextStyle(fontSize: 14.0),
                  ),
                  controlAffinity: ListTileControlAffinity.leading,
                ),
                SizedBox(height: 10),
                Container(
                  height: 40,
                  child: FlatButton(
                    color: Theme.of(context).accentColor,
                    child: Text(
                      'SUBMIT',
                      style: buttonTextStyle,
                    ),
                    onPressed: () {
                      if (_formKey.currentState.validate()) {
                        return contactUs();
                      }
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  contactUs() async {
    FeedBackForm feedBackForm = new FeedBackForm(
        name: nameController.text,
        email: emailController.text,
        phone: mobNoController.text,
        message: messageController.text,
        subject: subjectController.text);
    await feedBack(feedBackURL, map: feedBackForm.toMap());
    if (statusCode == 201) {
      _showDialog();
    }
  }
}
