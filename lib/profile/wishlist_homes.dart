import 'dart:convert';
import 'package:afh/home_screen/messages.dart';
import 'package:afh/third_page/call_back_request.dart';
import 'package:afh/third_page/tour_request.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import '../constant.dart';

class WishListHomes extends StatefulWidget {
  @override
  _WishListHomesState createState() => _WishListHomesState();
}

class _WishListHomesState extends State<WishListHomes> {
  List bookMarks = List();
  var bookMarksResponse;
  Set<String> bookedStayIds = Set();
  Future<String> getBookMarks() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bookMarksResponse = await http.get(
        Uri.encodeFull(BASE_URL + "/api/myafh/owner/mybookmarks"),
        headers: {
          "Accept": "application/json",
          "access-control-allow-origin": "*",
          "Authorization": "Bearer " + prefs.getString("token"),
        });
    setState(() {
      var jsonData = json.decode(bookMarksResponse.body);
      bookMarks = jsonData['listBookMarks'];
      bookMarks.forEach((home) => {bookedStayIds.add(home['stayId'])});
    });
    return "Success";
  }

  Future<String> unFavouriteHome(String stayId) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var bookMarksResponse = await http.get(
        Uri.encodeFull(
            BASE_URL + "/api/myafh/owner/unBookMarkHome?stayId=" + stayId),
        headers: {
          "Accept": "application/json",
          "access-control-allow-origin": "*",
          "Authorization": "Bearer " + prefs.getString("token"),
        });
    setState(() {
      var jsonData = json.decode(bookMarksResponse.body);
      jsonData['result'] == 'success' ? bookedStayIds.remove(stayId) : null;
    });
    print(bookedStayIds);
    return "Success";
  }

  void unBook(String stayId) {
    if (bookedStayIds.contains(stayId)) {
      unFavouriteHome(stayId);
    }
  }

  @override
  void initState() {
    super.initState();
    this.getBookMarks();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: BackButton(
          color: Colors.white,
        ),
        title: Text(
          'Wishlist',
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: bookMarksResponse == null
          ? Center(
              child: Text('Loading...'),
            )
          : bookMarks.isEmpty
              ? Card(
                  child: Center(
                    child: Text('No Favourite Homes found'),
                  ),
                )
              : ListView.builder(
                  padding: EdgeInsets.all(5),
                  itemBuilder: (BuildContext context, int index) {
                    return Card(
                      elevation: 10,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Stack(
                            children: <Widget>[
                              ClipRRect(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(4)),
                                child: Image.network(
                                  bookMarks[index]['pgImage'],
                                  fit: BoxFit.fill,
                                  height: 200,
                                  width: 350,
                                ),
                              ),
                              Positioned(
                                right: 5,
                                top: 5,
                                child: IconButton(
                                  icon: Icon(
                                    Icons.clear,
                                    size: 30,color: Colors.white,
                                  ),

                                  onPressed: () {
                                    setState(() {
                                      unBook(bookMarks[index]['stayId']);
                                      bookMarks.removeAt(index);
                                    });
                                  },
                                ),
                              ),
                            ],
                          ),
                          Container(
                            padding: EdgeInsets.only(left: 5, top: 5),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  bookMarks[index]['pgName'],
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16),
                                ),
                                SizedBox(
                                  height: 3,
                                ),
                                Text(
                                  bookMarks[index]['pgAddress'],
                                  style: TextStyle(color: Colors.grey),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          Container(
                            padding: EdgeInsets.all(5),
                            child: Row(
                              children: <Widget>[
                                Expanded(
                                  child: Container(
                                    height: 40,
                                    child: FlatButton(
                                      onPressed: () {
                                        Navigator.push(
                                          context,
                                          PageTransition(
                                            type:
                                                PageTransitionType.rightToLeft,
                                            child: MessageScreen(),
                                          ),
                                        );
                                      },
                                      color: Theme.of(context).accentColor,
                                      child: Text(
                                        'Message',
                                        style: buttonTextStyle,
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  width: 2,
                                ),
                                Expanded(
                                  child: Container(
                                    height: 40,
                                    child: FlatButton(
                                      onPressed: () {
                                        Navigator.push(
                                          context,
                                          PageTransition(
                                            type:
                                                PageTransitionType.rightToLeft,
                                            child: TourRequest(),
                                          ),
                                        );
                                      },
                                      color: Theme.of(context).accentColor,
                                      child: Text(
                                        'Tour Home',
                                        style: buttonTextStyle,
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  width: 2,
                                ),
                                Expanded(
                                  child: Container(
                                    height: 40,
                                    child: FlatButton(
                                      onPressed: () {
                                        Navigator.push(
                                          context,
                                          PageTransition(
                                            type:
                                                PageTransitionType.rightToLeft,
                                            child: CallBackRequest(),
                                          ),
                                        );
                                      },
                                      color: Theme.of(context).accentColor,
                                      child: Text(
                                        'Callback',
                                        style: buttonTextStyle,
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    );
                  },
                  itemCount: bookMarks == null ? 0 : bookMarks.length,
                ),
    );
  }
}
