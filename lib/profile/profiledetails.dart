import 'dart:convert';
import 'package:afh/model/profile_update.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../constant.dart';

class ProDetail extends StatefulWidget {
  @override
  _ProDetailState createState() => _ProDetailState();
}

class _ProDetailState extends State<ProDetail> {
  var _formKey = GlobalKey<FormState>();
  var statusCode;
  String profileEditURL = BASE_URL + "/api/myafh/owner/updateOwnerProfile";
  TextEditingController nameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController mobNoController = TextEditingController();
  TextEditingController cityController = TextEditingController();
  TextEditingController addressController = TextEditingController();
  TextEditingController bioController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

 // File _image;
  /*Future takeImageFromCamera() async {
    var image = await ImagePicker.pickImage(source: ImageSource.camera);
    setState(() {
      _image = image;
    });
  }
*/
  Future<ProfileUpdate> profile(String url, {map}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return http.post(url, body: utf8.encode(json.encode(map)), headers: {
      "content-type": "application/json",
      "Accept": "application/json",
      "access-control-allow-origin": "*",
      "Authorization": "Bearer " + prefs.getString("token"),
    }).then((http.Response response) {
      statusCode = response.statusCode;

      return ProfileUpdate.fromJson(json.decode(response.body));
    });
  }

  Future<void> _showDialog() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Your profile updated successfuly'),
          actions: <Widget>[
            FlatButton(
              child: Text(
                'Okay',
                style: TextStyle(color: Colors.white),
              ),
              color: Theme.of(context).accentColor,
              onPressed: () {
                Navigator.pop(context);
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: BackButton(
          color: Colors.white,
        ),
        title: Text(
          'Edit Profile',
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: Padding(
            padding: EdgeInsets.all(10),
            child: Column(
              children: <Widget>[
                Stack(
                  children: <Widget>[
                    CircleAvatar(
                      backgroundImage: NetworkImage(
                          'https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png'),
                      maxRadius: 60,
                    ),
                    Positioned(
                      bottom: 5,
                      right: 5,
                      child: IconButton(
                        icon: Icon(
                          Icons.camera_alt,
                          size: 50,
                          color: Theme.of(context).primaryColorDark,
                        ),
                        onPressed: () {
                          //takeImageFromCamera();
                        },
                      ),
                    )
                  ],
                ),
                SizedBox(
                  height: 15,
                ),
                TextFormField(
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    hintText: 'Name',
                    hintStyle: TextStyle(fontSize: 14),
                  ),
                  validator: (String value) {
                    if (value.isEmpty) {
                      return "Please enter name";
                    } else {
                      return null;
                    }
                  },
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    hintText: 'Email',
                    hintStyle: TextStyle(fontSize: 14),
                  ),
                  validator: (String value) {
                    if (value.isEmpty) {
                      return 'Please enter email';
                    } else {
                      return null;
                    }
                  },
                ),
                SizedBox(height: 10),
                TextFormField(
                  keyboardType: TextInputType.number,
                  maxLength: 10,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    hintText: 'Mobile Number',
                    hintStyle: TextStyle(fontSize: 14),
                  ),
                  validator: (String value) {
                    if (value.isEmpty) {
                      return 'Please enter mobile number';
                    } else {
                      return null;
                    }
                  },
                ),
                SizedBox(height: 10),
                TextFormField(
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    hintText: 'City',
                    hintStyle: TextStyle(fontSize: 14),
                  ),
                  validator: (String value) {
                    if (value.isEmpty) {
                      return 'Please enter last name';
                    } else {
                      return null;
                    }
                  },
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    hintText: 'Address',
                    hintStyle: TextStyle(fontSize: 14),
                  ),
                  validator: (String value) {
                    if (value.isEmpty) {
                      return 'Please enter address';
                    } else {
                      return null;
                    }
                  },
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    hintText: 'Biography',
                    hintStyle: TextStyle(fontSize: 14),
                  ),
                  maxLines: 2,
                  validator: (String value) {
                    if (value.isEmpty) {
                      return 'Please enter biography';
                    } else {
                      return null;
                    }
                  },
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    hintText: 'Current Password',
                    hintStyle: TextStyle(fontSize: 14),
                  ),
                  validator: (String value) {
                    if (value.isEmpty) {
                      return 'Please enter password';
                    } else {
                      return null;
                    }
                  },
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  height: 40,
                  width: double.infinity,
                  child: FlatButton(
                    color: Theme.of(context).accentColor,
                    onPressed: () {
                      if (_formKey.currentState.validate()) {
                        return profileEdit();
                      }
                    },
                    child: Text(
                      'SAVE',
                      style: buttonTextStyle,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  profileEdit() async {
    ProfileUpdate profileUpdate = new ProfileUpdate(
        ownerName: nameController.text,
        ownerEmail: emailController.text,
        ownerMobile: mobNoController.text,
        ownerCity: cityController.text,
        ownerAddress: addressController.text,
        bio: bioController.text,
        currentPassword: passwordController.text);
    await profile(profileEditURL, map: profileUpdate.toMap());
    if (statusCode == 200) {
      _showDialog();
    }
  }
}
