import 'dart:convert';
import 'package:afh/third_page/checkout.dart';
import 'package:afh/third_page/third_page.dart';
import 'package:afh/third_page/tour_request.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

import '../constant.dart';


class UserCallBackRequest extends StatefulWidget {
  @override
  _UserCallBackRequestState createState() => _UserCallBackRequestState();
}

class _UserCallBackRequestState extends State<UserCallBackRequest> {
  List callBack = List();
  var callbackResponse;
  Future<String> getOwnerCallBack() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    callbackResponse = await http.get(
        Uri.encodeFull(BASE_URL + "/api/myafh/owner/getOwnerCallBacks"),
        headers: {
          "Accept": "application/json",
          "access-control-allow-origin": "*",
          "Authorization": "Bearer " + prefs.getString("token"),
        });
    print(callbackResponse.statusCode);
    setState(() {
      var jsonData = json.decode(callbackResponse.body);
      callBack = jsonData['listCallBack'];
      print(callBack);
    });
    return "Success";
  }

  @override
  void initState() {
    super.initState();
    this.getOwnerCallBack();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: BackButton(
          color: Colors.white,
        ),
        title: Text(
          'Callback Request',
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: callbackResponse == null
          ? Center(
              child: Text('Loading...'),
            )
          : callBack.isEmpty
              ? Card(
                  child: Center(
                    child: Text('No CallBack Request found'),
                  ),
                )
              : ListView.builder(
                  padding: EdgeInsets.all(5),
                  itemBuilder: (BuildContext context, int index) {
                    return Card(
                      elevation: 10,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          ClipRRect(
                            borderRadius: BorderRadius.all(Radius.circular(4)),
                            child: Image.network(
                              callBack[index]['image'],
                              fit: BoxFit.fill,
                              height: 180,
                              width: 350,
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.only(left: 5, top: 5),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  callBack[index]['homeName'],
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16),
                                ),
                                SizedBox(
                                  height: 3,
                                ),
                                Text(
                                  callBack[index]['address'],
                                  style: TextStyle(color: Colors.grey),
                                ),
                                SizedBox(
                                  height: 3,
                                ),
                                Row(
                                  children: <Widget>[
                                    Text(
                                      'Date             :  ',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold),
                                    ),
                                    Text(
                                      callBack[index]['requestDate'],
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: 3,
                                ),
                              ],
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.all(5),
                            child: Row(
                              children: <Widget>[
                                Expanded(
                                  child: Container(
                                    height: 40,
                                    child: FlatButton(
                                      onPressed: () {
                                        Navigator.push(
                                          context,
                                          PageTransition(
                                            type:
                                                PageTransitionType.rightToLeft,
                                            child: ThirdPage(
                                              stayId: callBack[index]['stayId'],
                                              homeName: callBack[index]
                                                  ['homeName'],
                                            ),
                                          ),
                                        );
                                      },
                                      color: Theme.of(context).accentColor,
                                      child: Text(
                                        'View Listing',
                                        style: TextStyle(color: Colors.white),
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  width: 2,
                                ),
                                Expanded(
                                  child: Container(
                                    height: 40,
                                    child: FlatButton(
                                      onPressed: () {
                                        Navigator.push(
                                          context,
                                          PageTransition(
                                            type:
                                                PageTransitionType.rightToLeft,
                                            child: CheckOut(),
                                          ),
                                        );
                                      },
                                      color: Theme.of(context).accentColor,
                                      child: Text(
                                        'Reserve Bed',
                                        style: TextStyle(color: Colors.white),
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  width: 2,
                                ),
                                Expanded(
                                  child: Container(
                                    height: 40,
                                    child: FlatButton(
                                      onPressed: () {
                                        Navigator.push(
                                          context,
                                          PageTransition(
                                            type:
                                                PageTransitionType.rightToLeft,
                                            child: TourRequest(),
                                          ),
                                        );
                                      },
                                      color: Theme.of(context).accentColor,
                                      child: Text(
                                        'Tour Request',
                                        style: TextStyle(color: Colors.white),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    );
                  },
                  itemCount: callBack == null ? 0 : callBack.length,
                ),
    );
  }
}
