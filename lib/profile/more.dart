
import 'package:afh/help_center/helpcenter.dart';
import 'package:afh/profile/profiledetails.dart';
import 'package:afh/profile/user_callback_request.dart';
import 'package:afh/settings/settings.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'deposit_history.dart';
import 'faq.dart';
import 'feedback.dart';
import 'how_it_works.dart';
import 'loginpage.dart';

import 'user_tour_request.dart';
import 'wishlist_homes.dart';

class More extends StatefulWidget {
  @override
  _MoreState createState() => _MoreState();
}

class _MoreState extends State<More> {
  String name;
  String uid;

  @override
  void initState() {
    super.initState();
    checkStatus();
  }

  checkStatus() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (prefs.getString('ownername') != null) {
      setState(() {
        name = prefs.getString('ownername');
        uid = prefs.getString('userID');
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              name != null
                  ? GestureDetector(
                      onTap: () {
                        Navigator.push(
                          context,
                          PageTransition(
                            type: PageTransitionType.rightToLeft,
                            child: ProDetail(),
                          ),
                        );
                      },
                      child: Container(
                        color: Theme.of(context).primaryColor,
                        padding: EdgeInsets.only(left: 40),
                        width: double.infinity,
                        height: 150,
                        child: Row(
                          children: <Widget>[
                            Row(
                              mainAxisSize: MainAxisSize.max,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: <Widget>[
                                CircleAvatar(
                                  backgroundImage: NetworkImage(
                                      'https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png'),
                                  maxRadius: 60,
                                ),
                              ],
                            ),
                            Container(
                              padding: EdgeInsets.only(left: 40),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text(
                                    'Welcome',
                                    style: TextStyle(
                                        fontSize: 25, color: Colors.white),
                                  ),
                                  SizedBox(height: 10),
                                  Text(name,
                                      style: TextStyle(
                                          fontSize: 16, color: Colors.white)),
                                  SizedBox(height: 10),
                                  Text(
                                    'Edit profile',
                                    style: TextStyle(color: Colors.white),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    )
                  : GestureDetector(
                      onTap: () {
                        Navigator.push(
                          context,
                          PageTransition(
                            type: PageTransitionType.rightToLeft,
                            child: LoginPage(),
                          ),
                        );
                      },
                      child: Container(
                        color: Theme.of(context).primaryColor,
                        padding: EdgeInsets.fromLTRB(20, 40, 0, 20),
                        width: double.infinity,
                        height: 150,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text('Login to Explore',
                                style: TextStyle(
                                    color: Colors.white, fontSize: 24)),
                            SizedBox(height: 20),
                            Row(
                              children: <Widget>[
                                Text(
                                  'Get Started',
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 16),
                                ),
                                SizedBox(width: 10),
                                Icon(
                                  Icons.arrow_forward_ios,
                                  color: Colors.white,
                                  size: 15,
                                ),
                              ],
                            )
                          ],
                        ),
                      ),
                    ),
              Container(
                width: double.infinity,
                color: Colors.grey[300],
                child: Column(
                  children: <Widget>[
                    uid != null
                        ? GestureDetector(
                            onTap: () => onTapTourRequest(context),
                            child: Container(
                              color: Colors.white,
                              child: ListTile(
                                leading: Icon(
                                  Icons.notifications,
                                  color: Theme.of(context).primaryColorDark,
                                ),
                                title: Text('Tour Request'),
                              ),
                            ),
                          )
                        : GestureDetector(
                            onTap: () => Navigator.push(
                              context,
                              PageTransition(
                                type: PageTransitionType.rightToLeft,
                                child: LoginPage(),
                              ),
                            ),
                            child: Container(
                              color: Colors.white,
                              child: ListTile(
                                leading: Icon(
                                  Icons.notifications,
                                  color: Theme.of(context).primaryColorDark,
                                ),
                                title: Text('Tour Request'),
                              ),
                            ),
                          ),
                    SizedBox(
                      height: 2,
                    ),
                    uid != null
                        ? GestureDetector(
                            onTap: () => onTapCallBackRequest(context),
                            child: Container(
                              color: Colors.white,
                              child: ListTile(
                                leading: Icon(
                                  Icons.remove_circle,
                                  color: Theme.of(context).primaryColorDark,
                                ),
                                title: Text('Callback Request'),
                              ),
                            ),
                          )
                        : GestureDetector(
                            onTap: () => Navigator.push(
                              context,
                              PageTransition(
                                type: PageTransitionType.rightToLeft,
                                child: LoginPage(),
                              ),
                            ),
                            child: Container(
                              color: Colors.white,
                              child: ListTile(
                                leading: Icon(
                                  Icons.remove_circle,
                                  color: Theme.of(context).primaryColorDark,
                                ),
                                title: Text('Callback Request'),
                              ),
                            ),
                          ),
                    SizedBox(
                      height: 2,
                    ),
                    uid != null
                        ? GestureDetector(
                            onTap: () => onTapDepositHistory(context),
                            child: Container(
                              color: Colors.white,
                              child: ListTile(
                                leading: Icon(
                                  Icons.history,
                                  color: Theme.of(context).primaryColorDark,
                                ),
                                title: Text('Deposit History'),
                              ),
                            ),
                          )
                        : GestureDetector(
                            onTap: () => Navigator.push(
                              context,
                              PageTransition(
                                type: PageTransitionType.rightToLeft,
                                child: LoginPage(),
                              ),
                            ),
                            child: Container(
                              color: Colors.white,
                              child: ListTile(
                                leading: Icon(
                                  Icons.history,
                                  color: Theme.of(context).primaryColorDark,
                                ),
                                title: Text('Deposit History'),
                              ),
                            ),
                          ),
                    SizedBox(
                      height: 2,
                    ),
                    uid != null
                        ? GestureDetector(
                            onTap: () => onTapFavorite(context),
                            child: Container(
                              color: Colors.white,
                              child: ListTile(
                                leading: Icon(
                                  Icons.favorite,
                                  color: Theme.of(context).primaryColorDark,
                                ),
                                title: Text('Favorites'),
                              ),
                            ),
                          )
                        : GestureDetector(
                            onTap: () => Navigator.push(
                              context,
                              PageTransition(
                                type: PageTransitionType.rightToLeft,
                                child: LoginPage(),
                              ),
                            ),
                            child: Container(
                              color: Colors.white,
                              child: ListTile(
                                leading: Icon(
                                  Icons.favorite,
                                  color: Theme.of(context).primaryColorDark,
                                ),
                                title: Text('Favorites'),
                              ),
                            ),
                          ),
                    SizedBox(
                      height: 2,
                    ),
                    /*uid != null
                        ? GestureDetector(
                            onTap: () => onTapBecomeProvider(context),
                            child: Container(
                              color: Colors.white,
                              child: ListTile(
                                leading: Icon(
                                  Icons.feedback,
                                  color: Theme.of(context).primaryColorDark,
                                ),
                                title: Text('Become a Provider'),
                              ),
                            ),
                          )
                        : GestureDetector(
                            onTap: () => Navigator.push(
                              context,
                              PageTransition(
                                type: PageTransitionType.rightToLeft,
                                child: LoginPage(),
                              ),
                            ),
                            child: Container(
                              color: Colors.white,
                              child: ListTile(
                                leading: Icon(
                                  Icons.feedback,
                                  color: Theme.of(context).primaryColorDark,
                                ),
                                title: Text('Become a Provider'),
                              ),
                            ),
                          ),
                    SizedBox(
                      height: 2,
                    ),
                    uid != null
                        ? GestureDetector(
                            onTap: () => onTapHomeOwner(context),
                            child: Container(
                              color: Colors.white,
                              child: ListTile(
                                leading: Icon(
                                  Icons.account_box,
                                  color: Theme.of(context).primaryColorDark,
                                ),
                                title: Text('Are you a HomeOwner'),
                              ),
                            ),
                          )
                        : GestureDetector(
                            onTap: () => Navigator.push(
                              context,
                              PageTransition(
                                  type: PageTransitionType.rightToLeft,
                                  child: LoginPage()),
                            ),
                            child: Container(
                              color: Colors.white,
                              child: ListTile(
                                leading: Icon(
                                  Icons.account_box,
                                  color: Theme.of(context).primaryColorDark,
                                ),
                                title: Text('Are you a HomeOwner'),
                              ),
                            ),
                          ),
                    SizedBox(
                      height: 2,
                    ),
                    uid != null
                        ? GestureDetector(
                            onTap: () => onTapCaregiver(context),
                            child: Container(
                              color: Colors.white,
                              child: ListTile(
                                leading: Icon(
                                  Icons.feedback,
                                  color: Theme.of(context).primaryColorDark,
                                ),
                                title: Text('Are you a Caregiver'),
                              ),
                            ),
                          )
                        : GestureDetector(
                            onTap: () => Navigator.push(
                              context,
                              PageTransition(
                                  type: PageTransitionType.rightToLeft,
                                  child: LoginPage()),
                            ),
                            child: Container(
                              color: Colors.white,
                              child: ListTile(
                                leading: Icon(
                                  Icons.feedback,
                                  color: Theme.of(context).primaryColorDark,
                                ),
                                title: Text('Are you a Caregiver'),
                              ),
                            ),
                          ),
                    SizedBox(
                      height: 2,
                    ),*/
                    GestureDetector(
                      onTap: () => onTapHelpCenter(context),
                      child: Container(
                        color: Colors.white,
                        child: ListTile(
                          leading: Icon(
                            Icons.help,
                            color: Theme.of(context).primaryColorDark,
                          ),
                          title: Text('Help Center & Feedback'),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 2,
                    ),
                    GestureDetector(
                      onTap: () => onTapFAQ(context),
                      child: Container(
                        color: Colors.white,
                        child: ListTile(
                          leading: Icon(
                            Icons.feedback,
                            color: Theme.of(context).primaryColorDark,
                          ),
                          title: Text('FAQ\'s'),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 2,
                    ),
                    GestureDetector(
                      onTap: () => onTapHowItWorks(context),
                      child: Container(
                        color: Colors.white,
                        child: ListTile(
                          leading: Icon(
                            Icons.feedback,
                            color: Theme.of(context).primaryColorDark,
                          ),
                          title: Text('How it works'),
                        ),
                      ),
                    ),
                    SizedBox(height: 2),
                    GestureDetector(
                      onTap: () => onTapSetting(context),
                      child: Container(
                        color: Colors.white,
                        child: ListTile(
                          leading: Icon(
                            Icons.settings,
                            color: Theme.of(context).primaryColorDark,
                          ),
                          title: Text('Settings'),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void onTapProfile(BuildContext context) {
    Navigator.push(
        context,
        PageTransition(
            type: PageTransitionType.rightToLeft, child: ProDetail()));
  }

  void onTapSetting(BuildContext context) {
    Navigator.push(context,
        PageTransition(type: PageTransitionType.rightToLeft, child: Setting()));
  }

  void onTapTourRequest(BuildContext context) {
    Navigator.push(
        context,
        PageTransition(
            type: PageTransitionType.rightToLeft, child: UserTourRequest()));
  }

  void onTapCallBackRequest(BuildContext context) {
    Navigator.push(
        context,
        PageTransition(
            type: PageTransitionType.rightToLeft,
            child: UserCallBackRequest()));
  }

  void onTapFeedback(BuildContext context) {
    Navigator.push(
        context,
        PageTransition(
            type: PageTransitionType.rightToLeft, child: FeedBackPage()));
  }

  void onTapHelpCenter(BuildContext context) {
    Navigator.push(
        context,
        PageTransition(
            type: PageTransitionType.rightToLeft, child: HelpCenterPage()));
  }

  void onTapFavorite(BuildContext context) {
    Navigator.push(
        context,
        PageTransition(
            type: PageTransitionType.rightToLeft, child: WishListHomes()));
  }

  void onTapFAQ(BuildContext context) {
    Navigator.push(context,
        PageTransition(type: PageTransitionType.rightToLeft, child: FAQTile()));
  }

  void onTapDepositHistory(BuildContext context) {
    Navigator.push(
        context,
        PageTransition(
            type: PageTransitionType.rightToLeft, child: DepositHistory()));
  }


  void onTapHowItWorks(BuildContext context) {
    Navigator.push(
        context,
        PageTransition(
            type: PageTransitionType.rightToLeft, child: HowItWorks()));
  }
}
