import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import '../constant.dart';


class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  bool obscureText = true;
  void _toggle() {
    setState(() {
      obscureText = !obscureText;
    });
  }

  final scaffoldKey = GlobalKey<ScaffoldState>();
  final TextEditingController emailController = new TextEditingController();
  final TextEditingController passwordController = new TextEditingController();
  bool _isLoading = false;
  var myData;
  void _login() async {
    final uri = BASE_URL + '/api/myafh/login?role=user';
    final headers = {'content-type': 'application/json'};
    Map<String, dynamic> body = {
      "username": emailController.text,
      "password": passwordController.text,
      "role": "user",
    };
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String jsonBody = json.encode(body);
    final encoding = Encoding.getByName('utf-8');
    var response = await http.post(uri,
        headers: headers, body: jsonBody, encoding: encoding);
    if (response.statusCode == 200 &&
        json.decode(response.body)['status'] != 'failed') {
      myData = json.decode(response.body);
      setState(() {
        prefs.setString('token', myData['token']);
      });
      getAllUsers();
    } else if (json.decode(response.body)['status'] == 'failed') {
      setState(() {
        _isLoading = false;
      });
      showInSnackBar('Please enter valid email and password');
    }
  }

  void showInSnackBar(String value) {
    Scaffold.of(context).showSnackBar(new SnackBar(content: new Text(value)));
  }

  Future<String> getAllUsers() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var response = await http.get(
        Uri.encodeFull(BASE_URL + "/api/myafh/owner/getUserProfiles"),
        headers: {
          "Accept": "application/json",
          ''
              "access-control-allow-origin": "*",
          "Authorization": "Bearer " + prefs.getString("token"),
        });
    if (response.statusCode == 200) {
      var convertedData = json.decode(response.body);
      var profileData = convertedData['ownerProfile'];
      prefs.setString('ownername', profileData['ownerName']);
      prefs.setString('userID', profileData['uid']);
      prefs.setString('email', profileData['email']);
      prefs.setString('mobNo', profileData['mobile']);
      Navigator.pop(context);
    }
    return "Success";
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
              colors: [Color(0xFF538DB8), Color(0xFF97C160)],
              begin: Alignment.topLeft,
              end: Alignment.bottomRight),
        ),
        child: _isLoading
            ? Center(child: CircularProgressIndicator())
            : Column(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(top: 70.0),
                    padding:
                        EdgeInsets.symmetric(horizontal: 20.0, vertical: 30.0),
                    child: Text("Adult Family Homes",
                        style: TextStyle(
                            color: Colors.white70,
                            fontSize: 30.0,
                            fontWeight: FontWeight.bold)),
                  ),
                  Container(
                    padding:
                        EdgeInsets.symmetric(horizontal: 15.0, vertical: 20.0),
                    child: Column(
                      children: <Widget>[
                        TextFormField(
                          controller: emailController,
                          cursorColor: Colors.white,
                          style: TextStyle(color: Colors.white70),
                          decoration: InputDecoration(
                            icon: Icon(Icons.email, color: Colors.white70),
                            hintText: "Email",
                            border: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.white70)),
                            hintStyle: TextStyle(color: Colors.white70),
                          ),
                          keyboardType: TextInputType.emailAddress,
                        ),
                        SizedBox(height: 30.0),
                        TextFormField(
                          controller: passwordController,
                          cursorColor: Colors.white,
                          obscureText: obscureText,
                          style: TextStyle(color: Colors.white70),
                          decoration: InputDecoration(
                            icon: Icon(Icons.lock, color: Colors.white70),
                            hintText: "Password",
                            border: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.white70)),
                            hintStyle: TextStyle(color: Colors.white70),
                            suffixIcon: IconButton(
                              onPressed: _toggle,
                              icon: Icon(obscureText
                                  ? Icons.visibility
                                  : Icons.visibility_off),
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: 40.0,
                    padding: EdgeInsets.symmetric(horizontal: 15.0),
                    margin: EdgeInsets.only(top: 15.0),
                    child: RaisedButton(
                      onPressed: emailController.text == "" ||
                              passwordController.text == ""
                          ? null
                          : () {
                              setState(() {
                                _isLoading = true;
                              });
                              _login();
                            },
                      elevation: 0.0,
                      color: Colors.purple,
                      child: Text("Log In",
                          style: TextStyle(color: Colors.white70)),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5.0),
                      ),
                    ),
                  ),
                ],
              ),
      ),
    );
  }
}
